<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Boleto extends Model
{
    protected $table = 'boletos';

    protected $fillable = [
        'orden_id', 'planta', 'fila', 'numero', 'fecha', 'proximoCorte', 'status',
    ];

    public function ordenCompra() {
        return $this->belongsTo('App\Models\OrdenCompra', 'orden_id');
    }
}
