<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{
    protected $table = 'ordenes_compra';

    protected $fillable = [
        'name', 'email', 'telefono', 'fechaCompra', 'cantidad', 'planta', 'metodoPago', 'monto', 'status', 'paymentID', 'transactionID', 'invoiceNumber',
    ];

    public function boletos() {
        return $this->hasMany('App\Models\Boleto', 'orden_id');
    }
}
