<?php

namespace App\PaymentMethods;

use App\Models\OrdenCompra;
use Illuminate\Http\Request;
use MercadoPago\Item;
use MercadoPago\MerchantOrder;
use MercadoPago\Payer;
use MercadoPago\Payment;
use MercadoPago\Preference;
use MercadoPago\SDK;

class MercadoPago
{
  public function __construct()
  {
    // SDK::setClientId(
    //   config("payment-methods.mercadopago.client")
    // );
    // SDK::setClientSecret(
    //   config("payment-methods.mercadopago.secret")
    // );
    SDK::setAccessToken(
      config("payment-methods.mercadopago.secret")
    );
  }

    
  public function setupPaymentAndGetRedirectURL(OrdenCompra $order)
  {
     # Create a preference object
     $preference = new Preference();

      # Create an item object
      $monto = $order->planta == '1' ? 1600 : 1200;
      $item = new Item();
      $item->id = 1;
      $item->title = 'Compra de Boletos';
      $item->quantity = $order->cantidad;
      $item->currency_id = 'MXN';
      $item->unit_price = $monto;

      # Create a payer object
      $payer = new Payer();
      $payer->name = $order->name;
      $payer->email = $order->correo;

      # Setting preference properties
      $preference->items = [$item];
      $preference->payer = $payer;

      # Save External Reference
      $preference->external_reference = $order->id;
      $preference->back_urls = [
        "success" => route('boletos.asignar', $order), // Asisgnacion de boletos
        "failure" => route('home'),
      ];
        
      $preference->auto_return = "all";

      # Excludes Payment Methods
      $preference->payment_methods = array(
        "excluded_payment_types" => array(
          array("id" => "ticket"),
          array("id" => "atm")
        ),
        "installments" => 1
      );

      $preference->binary_mode = true;
      # Save and POST preference
      $preference->save();

      if (false) {
        return $preference->sandbox_init_point;
      }

      return $preference->init_point;
  }
}