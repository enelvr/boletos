<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrdenCompra;
use App\Models\Boleto;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Boletos totales por planta
        $boletosZ0 = Boleto::where('planta', 'ZONA 0')->count();
        $boletosZ1 = Boleto::where('planta', 'ZONA 1')->count();
        $boletosZ2 = Boleto::where('planta', 'ZONA 2')->count();

        // Boletos venvidos por planta
        $boletosVendidosZ0 = Boleto::where('orden_id', '!=', null)->where('planta', 'ZONA 0')->count();
        $boletosVendidosZ1 = Boleto::where('orden_id', '!=', null)->where('planta', 'ZONA 1')->count();
        $boletosVendidosZ2 = Boleto::where('orden_id', '!=', null)->where('planta', 'ZONA 2')->count();

        // Boletos disponibles por planta
        $boletosDisponiblesZ0 = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA 0')->count();
        $boletosDisponiblesZ1 = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA 1')->count();
        $boletosDisponiblesZ2 = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA 2')->count();

        // Boletos bloqueados por planta
        $boletosBloqueadosZ1 = Boleto::where('planta', 'ZONA 1')->where('status', 'bloqueado')->count();
        $boletosBloqueadosZ2 = Boleto::where('planta', 'ZONA 2')->where('status', 'bloqueado')->count();

        $montoRecaudado = ( $boletosVendidosZ1 * 1600 ) + ( $boletosVendidosZ2 * 1200 );

        return view('admin.dashboard.index', compact('boletosZ0', 'boletosZ1', 'boletosZ2', 'boletosVendidosZ0', 'boletosVendidosZ1', 'boletosVendidosZ2', 'boletosDisponiblesZ0', 'boletosDisponiblesZ1', 'boletosDisponiblesZ2', 'montoRecaudado', 'boletosBloqueadosZ1', 'boletosBloqueadosZ2'));
    }

    public function clientes()
    {
        $compras = OrdenCompra::where('status', 'Completada')->get();

        return view('admin.dashboard.clientes', compact('compras'));
    }

    public function boletos()
    {
        // Boletos disponibles por planta
        $boletosDisponiblesZ1 = Boleto::where('orden_id', null)->where('planta', 'ZONA 1')->count();
        $boletosDisponiblesZ2 = Boleto::where('orden_id', null)->where('planta', 'ZONA 2')->count();
        $disponibles = json_encode([$boletosDisponiblesZ1, $boletosDisponiblesZ2]);
        $disponibles2 = [$boletosDisponiblesZ1, $boletosDisponiblesZ2];
        return view('admin.dashboard.boletos', compact('disponibles', 'disponibles2'));
    }

    public function boletosGrafico()
    {
        $filasZ1 = [
            ['fila' => 'AA', 'asientos' => 24, 'cortes' => [8, 16]],
            ['fila' => 'A', 'asientos' => 27, 'cortes' => [9, 18]],
            ['fila' => 'B', 'asientos' => 27, 'cortes' => [9, 18]],
            ['fila' => 'C', 'asientos' => 28, 'cortes' => [9, 19]],
            ['fila' => 'D', 'asientos' => 10, 'cortes' => []],
            ['fila' => 'E', 'asientos' => 29, 'cortes' => [9, 20]],
            ['fila' => 'F', 'asientos' => 29, 'cortes' => [9, 20]],
            ['fila' => 'G', 'asientos' => 30, 'cortes' => [9, 21]],
            ['fila' => 'H', 'asientos' => 30, 'cortes' => [9, 21]],
            ['fila' => 'I', 'asientos' => 31, 'cortes' => [9, 22]],
            ['fila' => 'J', 'asientos' => 31, 'cortes' => [9, 22]],
            ['fila' => 'K', 'asientos' => 32, 'cortes' => [9, 23]],
            ['fila' => 'L', 'asientos' => 32, 'cortes' => [9, 23]],
            ['fila' => 'M', 'asientos' => 31, 'cortes' => [8, 23]],
            ['fila' => 'N', 'asientos' => 31, 'cortes' => [8, 23]],
            ['fila' => 'O', 'asientos' => 32, 'cortes' => [8, 24]],
            ['fila' => 'P', 'asientos' => 32, 'cortes' => [8, 24]],
            ['fila' => 'Q', 'asientos' => 31, 'cortes' => [7, 24]],
            ['fila' => 'R', 'asientos' => 31, 'cortes' => [7, 24]],
            ['fila' => 'S', 'asientos' => 32, 'cortes' => [7, 25]],
            ['fila' => 'T', 'asientos' => 31, 'cortes' => [6, 25]],
            ['fila' => 'U', 'asientos' => 17, 'cortes' => []],
            ['fila' => 'V', 'asientos' => 13, 'cortes' => []],
            ['fila' => 'A', 'asientos' => 4, 'cortes' => [2]],
            ['fila' => 'B', 'asientos' => 4, 'cortes' => [2]],
            ['fila' => 'C', 'asientos' => 4, 'cortes' => [2]],
            ['fila' => 'D', 'asientos' => 4, 'cortes' => [2]],
            ['fila' => 'E', 'asientos' => 33, 'cortes' => [6, 27]],
            ['fila' => 'F', 'asientos' => 33, 'cortes' => [6, 27]],
        ];

        $filasZ2 = [
            ['fila' => 'G', 'asientos' => 33, 'cortes' => [6, 27]],
            ['fila' => 'H', 'asientos' => 21, 'cortes' => [6, 15]],
            ['fila' => 'I', 'asientos' => 22, 'cortes' => [6, 17]],
            ['fila' => 'J', 'asientos' => 22, 'cortes' => [6, 17]],
            ['fila' => 'K', 'asientos' => 22, 'cortes' => [5, 17]],
            ['fila' => 'L', 'asientos' => 22, 'cortes' => [5, 17]],
            ['fila' => 'M', 'asientos' => 26, 'cortes' => [7, 20]],
            ['fila' => 'N', 'asientos' => 34, 'cortes' => [11, 24]],
            ['fila' => 'O', 'asientos' => 33, 'cortes' => [10, 24]],
            ['fila' => 'P', 'asientos' => 33, 'cortes' => [10, 24]],
            ['fila' => 'Q', 'asientos' => 34, 'cortes' => [10, 25]],
            ['fila' => 'R', 'asientos' => 34, 'cortes' => [10, 25]],
            ['fila' => 'S', 'asientos' => 26, 'cortes' => [13]],
        ];

        $filas1 = [];
        for ($i = 0; $i < sizeof($filasZ1); $i++) {

            $fila = $filasZ1[$i]['fila'];
            $boletos = [];

            for($j = 0; $j < $filasZ1[$i]['asientos']; $j++) {
                $numero = $j + 1;
                $bloqueado = 'bloqueado';
                $orden = null;

                $flag = $i == 11 && ($j >= 0 && $j <= 3);
                if ($i >= 11 && !$flag) {
                    $boleto = Boleto::where('planta', 'ZONA 1')
                                    ->where('fila', $fila)
                                    ->where('numero', $numero)
                                    ->get();
                    if (sizeof($boleto) != 0) {
                        if ($boleto[0]->status == 'bloqueado') {
                            $bloqueado = 'bloqueado2';
                        } else {
                            $bloqueado = $boleto[0]->orden_id == null ? 'disponible' : 'vendido';
                        }
                        $orden = $boleto[0]->ordenCompra;
                        if (isset($orden)) {
                            $asignados = '';
                            foreach ($orden->boletos as $boleto) {
                                $asignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                            }
                            $orden = json_encode([
                                'id' => $orden->id,
                                'nombre' => $orden->name,
                                'email' => $orden->email,
                                'telefono' => $orden->telefono,
                                'cantidad' => $orden->cantidad,
                                'hora' => $orden->fechaCompra,
                                'zona' => $orden->planta,
                                'asignados' => $asignados,
                            ]);
                        }
                    }
                } else {
                    $boleto = Boleto::where('planta', 'ZONA 0')
                                    ->where('fila', $fila)
                                    ->where('numero', $numero)
                                    ->get();
                    if (sizeof($boleto) != 0) {
                        $bloqueado = $boleto[0]->orden_id == null ? 'bloqueado' : 'vendido';

                        $orden = $boleto[0]->ordenCompra;
                        if (isset($orden)) {
                            $asignados = '';
                            foreach ($orden->boletos as $boleto) {
                                $asignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                            }
                            $orden = json_encode([
                                'id' => $orden->id,
                                'nombre' => $orden->name,
                                'email' => $orden->email,
                                'telefono' => $orden->telefono,
                                'cantidad' => $orden->cantidad,
                                'hora' => $orden->fechaCompra,
                                'zona' => $orden->planta,
                                'asignados' => $asignados,
                            ]);
                        }
                    }
                }

                $corte = false;
                if(in_array($j + 1, $filasZ1[$i]['cortes'])) {
                    $corte = true;
                }

                $boletos[] = [
                    'numero' => $numero,
                    'bloqueado' => $bloqueado,
                    'orden' => $orden,
                ];

                if ($corte) {
                    $boletos[] = [
                        'numero' => 0,
                        'bloqueado' => false,
                        'orden' => $orden,
                    ];
                }
            }

            $filas1[] = [
                'fila' => $fila,
                'boletos' => $boletos,
            ];
        }

        $filas2 = [];
        for ($i = 0; $i < sizeof($filasZ2); $i++) {

            $fila = $filasZ2[$i]['fila'];
            $boletos = [];

            for($j = 0; $j < $filasZ2[$i]['asientos']; $j++) {
                $numero = $j + 1;
                $bloqueado = 'bloqueado';
                $orden = null;

                $boleto = Boleto::where('planta', 'ZONA 2')
                                ->where('fila', $fila)
                                ->where('numero', $numero)
                                ->get();
                if (sizeof($boleto) != 0) {
                    if ($boleto[0]->status == 'bloqueado') {
                        $bloqueado = 'bloqueado2';
                    } else {
                        $bloqueado = $boleto[0]->orden_id == null ? 'disponible' : 'vendido';
                    }
                    $orden = $boleto[0]->ordenCompra;
                    if (isset($orden)) {
                        $asignados = '';
                        foreach ($orden->boletos as $boleto) {
                            $asignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                        }
                        $orden = json_encode([
                            'id' => $orden->id,
                            'nombre' => $orden->name,
                            'email' => $orden->email,
                            'telefono' => $orden->telefono,
                            'cantidad' => $orden->cantidad,
                            'hora' => $orden->fechaCompra,
                            'zona' => $orden->planta,
                            'asignados' => $asignados,
                        ]);
                    }
                }

                $corte = false;
                if(in_array($j + 1, $filasZ2[$i]['cortes'])) {
                    $corte = true;
                }

                $boletos[] = [
                    'numero' => $numero,
                    'bloqueado' => $bloqueado,
                    'orden' => $orden,
                ];

                if ($corte) {
                    $boletos[] = [
                        'numero' => 0,
                        'bloqueado' => false,
                        'orden' => $orden,
                    ];
                }
            }

            $filas2[] = [
                'fila' => $fila,
                'boletos' => $boletos,
            ];
        }

        return view('admin.dashboard.boletosGrafico', compact('filas1', 'filas2'));
    }

    public function selectorBoletos(Request $request)
    {
        $formData = [
            'planta' => $request->planta,
            'nombre' => $request->nombre,
            'email' => $request->email,
            'telefono' => $request->telefono,
            'cantidad' => $request->cantidad,
        ];

        $filasZ0 = [
            ['fila' => 'AA', 'asientos' => 24, 'cortes' => [8, 16]],
            ['fila' => 'A', 'asientos' => 27, 'cortes' => [9, 18]],
            ['fila' => 'B', 'asientos' => 27, 'cortes' => [9, 18]],
            ['fila' => 'C', 'asientos' => 28, 'cortes' => [9, 19]],
            ['fila' => 'D', 'asientos' => 10, 'cortes' => []],
            ['fila' => 'E', 'asientos' => 29, 'cortes' => [9, 20]],
            ['fila' => 'F', 'asientos' => 29, 'cortes' => [9, 20]],
            ['fila' => 'G', 'asientos' => 30, 'cortes' => [9, 21]],
            ['fila' => 'H', 'asientos' => 30, 'cortes' => [9, 21]],
            ['fila' => 'I', 'asientos' => 31, 'cortes' => [9, 22]],
            ['fila' => 'J', 'asientos' => 31, 'cortes' => [9, 22]],
            ['fila' => 'K', 'asientos' => 4, 'cortes' => []],
        ];

        $filas0 = [];
        for ($i = 0; $i < sizeof($filasZ0); $i++) {

            $fila = $filasZ0[$i]['fila'];
            $boletos = [];

            for($j = 0; $j < $filasZ0[$i]['asientos']; $j++) {
                $numero = $j + 1;
                $bloqueado = 'bloqueado';
                $id = null;

                $boleto = Boleto::where('planta', 'ZONA 0')
                                ->where('fila', $fila)
                                ->where('numero', $numero)
                                ->get();
                if (sizeof($boleto) != 0) {
                    $bloqueado = $boleto[0]->orden_id == null ? 'disponible' : 'vendido';
                    $id = $boleto[0]->id;
                }

                $corte = false;
                if(in_array($j + 1, $filasZ0[$i]['cortes'])) {
                    $corte = true;
                }

                $boletos[] = [
                    'numero' => $numero,
                    'bloqueado' => $bloqueado,
                    'id' => $id
                ];

                if ($corte) {
                    $boletos[] = [
                        'numero' => 0,
                        'bloqueado' => false,
                        'id' => null,
                    ];
                }
            }

            $filas0[] = [
                'fila' => $fila,
                'boletos' => $boletos,
            ];
        }

        return view('admin.dashboard.selectorBoletos', compact('filas0', 'formData'));
    }
}
