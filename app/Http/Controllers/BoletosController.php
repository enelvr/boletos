<?php

namespace App\Http\Controllers;

use App\Models\OrdenCompra;
use App\Models\Boleto;
use Illuminate\Http\Request;
use App\Mail\BoletosEmail;
use Illuminate\Support\Facades\Mail;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\RedirectUrls;
use PayPal\Api\InputFields;
use PayPal\Api\WebProfile;

use Barryvdh\DomPDF\Facade as PDF;
use Storage;
use \Milon\Barcode\DNS1D;

use DB;


class BoletosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $apiContext;
    public $sandbox;

    public function __construct()
    {
        // $this->middleware('auth');
        $this->apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AU3ru8QwnWcrliywkwlC8fKYzPUqhjbNwsnhDx0GSO1h5NzNSm61TP7ljP7yE5ouqojMUKy-vWOj0ayq', //ClientID
                'EOc9QUuu4_M7m_ZTx_jWUfHcbyZJLtwgK9KA1w7a9n0LsMC-y0lY48SUJfAPCcLFLSMuY6wEvX_ajjBs' //ClientSecret
            )
        );

        // true = sandbox, false = live
        $this->sandbox = false;
    }

    public function boletosBuy()
    {
        // Boletos disponibles por planta
        $boletosDisponiblesZ1 = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA 1')->count();
        $boletosDisponiblesZ2 = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA 2')->count();
        $disponibles = json_encode([$boletosDisponiblesZ1, $boletosDisponiblesZ2]);
        $disponibles2 = [$boletosDisponiblesZ1, $boletosDisponiblesZ2];

        if ($boletosDisponiblesZ1 != 0 || $boletosDisponiblesZ2 != 0) {
            return view('front.page.buy.index', compact('disponibles', 'disponibles2'));
        }
        return view('front.page.buy.agotados');
    }

    protected function generatePaymentGateway($paymentMethod, $orden) : string
    {
        $method = new \App\PaymentMethods\MercadoPago;

        return $method->setupPaymentAndGetRedirectURL($orden);
    }

    public function comprar(Request $request)
    {
        if ($request->cantidad < 1 || $request->cantidad > 8 || is_float($request->cantidad)) {
            return redirect()->back()->with('errors', 'Cantidad debe ser un valor entre 1 y 8');
        }
        $monto = $request->planta == '1' ? 1600 : 1200;
        // Crear Orden de compra
        $orden = new OrdenCompra();
        $orden->name = $request->nombre;
        $orden->email = $request->email;
        $orden->telefono = $request->telefono;
        $orden->planta = $request->planta;
        $orden->metodoPago = 'mercadoPago';
        $orden->cantidad = $request->cantidad;
        $orden->monto = $request->cantidad * $monto;
        $orden->status = "En proceso";
        $orden->fechaCompra = date('Y-m-d H:i:s');
        $orden->save();

        $url = $this->generatePaymentGateway(
            $orden->metodoPago, $orden
        );

        return redirect()->to($url);
    }

    public function asignarBoletos(OrdenCompra $orden)
    {
        if ($orden->status !== 'Completada') {

            $orden->status = "Completada";
            $orden->save();

            if ($orden->metodoPago === 'mercadoPago') {
                $orden->paymentID = $_GET["preference_id"];
                $orden->transactionID = $_GET["merchant_order_id"];
                $orden->invoiceNumber = $_GET["collection_id"];
            } else if ($orden->metodoPago === 'Paypal') {
                $orden->paymentID = $_GET["paymentId"];
                $orden->transactionID = $_GET["transaction"];
                $orden->invoiceNumber = $_GET["invoice"];
            }
            $orden->save();

            $boletos = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA ' . $orden->planta)->get();

            $lastBoletoid = null;
            $boletosAsignados = '';
            $boletosArr = [];
            $corte = false;
            for ($i = 0; $i < sizeof($boletos); $i++) {

                if ($corte) {
                    $lastBoletoid = null;
                    $boletosAsignados = '';
                    $boletosArr = [];
                    $corte = false;
                }

                $boleto = $boletos[$i];
                if ($lastBoletoid == null) {
                    $corte = $boleto->proximoCorte;
                    $lastBoletoid = $boleto->id;
                    $boleto->orden_id = $orden->id;
                    $boletosArr[] = $boleto;
                    $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                } else {
                    if ($lastBoletoid == ($boleto->id - 1)) {
                        $corte = $boleto->proximoCorte;
                        $lastBoletoid = $boleto->id;
                        $boleto->orden_id = $orden->id;
                        $boletosArr[] = $boleto;
                        $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                    } else {
                        $boletosArr = [];

                        $corte = $boleto->proximoCorte;
                        $lastBoletoid = $boleto->id;
                        $boleto->orden_id = $orden->id;
                        $boletosArr[] = $boleto;
                        $boletosAsignados = $boleto->fila .'-'. $boleto->numero . ' ';
                    }
                }

                if (sizeof($boletosArr) < $orden->cantidad) {
                    continue;
                } else {
                    break;
                }
            }
            foreach ($boletosArr as $boleto) {
                $boleto->save();
            }
        } else {
            $boletosAsignados = '';
            foreach ($orden->boletos as $boleto) {
                $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
            }
        }

        // Send mail
        $boletosObj = new \stdClass();
        $boletosObj->ordenid = $orden->id;
        $boletosObj->comprador = $orden->name;
        $boletosObj->planta = $orden->planta;
        $boletosObj->asientos = $boletosAsignados;

        $barra = new DNS1D();
        // $pdf = PDF::loadView('front.pdf.boletos', compact('orden', 'barra'));
        // return $pdf->stream('boletos.pdf');
        $pdf = PDF::loadView('front.pdf.boletos', compact('orden', 'barra'))->save(storage_path('app\public\boletos-'.$orden->id.'.pdf'));

        Mail::to($orden->email)->send(new BoletosEmail($boletosObj));

        $orden_arr = [
            'comprador' => $orden->name,
            'planta' => ucfirst($orden->planta),
            'ordenid' => $orden->id,
            'boletosAsignados' => $boletosAsignados,
        ];

        $url = route('compra.fin');
        return redirect()->to($url)->with('orden', $orden_arr);
    }

    public function compraFin () {
        $orden = session('orden');
        if (!isset($orden)) {
            return redirect()->route('home');
        }

        $comprador = $orden['comprador'];
        $planta = $orden['planta'];
        $ordenid = $orden['ordenid'];
        $boletosAsignados = $orden['boletosAsignados'];

        return view('front.page.buy.success', compact('comprador', 'planta', 'boletosAsignados', 'ordenid'));
    }

    public function downloadBoletos (Request $request) {
        $orden = OrdenCompra::find($request->ordenid);
        $barra = new DNS1D();
        $pdf = PDF::loadView('front.pdf.boletos', compact('orden', 'barra'));
        return $pdf->download('boletos.pdf');
    }

    public function crearPaymentPP (Request $request) {
        $boletosDisponiblesZ1 = Boleto::where('orden_id', null)->where('planta', 'ZONA 1')->count();
        $boletosDisponiblesZ2 = Boleto::where('orden_id', null)->where('planta', 'ZONA 2')->count();
        $disponibles = [$boletosDisponiblesZ1, $boletosDisponiblesZ2];
        $selected = $request->planta;
        $disponible = $disponibles[$selected - 1];
        if ($disponible < 8) {
            $max = $disponible;
        } else {
            $max = 8;
        }
        if ($request->cantidad > $max || $request->cantidad < 1) {
            return 'Ingrese un valor de cantidad entre 1 y '. $max;
        }

        $apiContext = $this->apiContext;

        $sandbox = $this->sandbox;

        if (!$sandbox) {
            $apiContext->setConfig(
                array(
                  'mode' => 'live',
                )
            );
        }

        // if (is_float($request->cantidad)) {
        //     return 'Cantidad debe ser un valor entero';
        // }
        $monto = $request->planta == '1' ? 1600 : 1200;
        // Crear Orden de compra
        $orden = new OrdenCompra();
        $orden->name = $request->nombre;
        $orden->email = $request->email;
        $orden->telefono = $request->telefono;
        $orden->planta = $request->planta;
        $orden->metodoPago = 'Paypal';
        $orden->cantidad = $request->cantidad;
        $orden->monto = $request->cantidad * $monto;
        $orden->status = "En proceso";
        $orden->fechaCompra = date('Y-m-d H:i:s');
        $orden->save();

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item1 = new Item();
        $item1->setName('Boletos Zona '. $request->planta)
            ->setCurrency('MXN')
            ->setQuantity($request->cantidad)
            ->setSku("1") // Similar to `item_number` in Classic API
            ->setPrice($monto);

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        $amount = new Amount();
        $amount->setCurrency("MXN")
            ->setTotal($request->cantidad * $monto);
        
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Boletos Zona ". ucfirst($request->planta))
            ->setInvoiceNumber(uniqid());

        $url = route('boletos.asignar', $orden);
        $urlcancel = route('boletos');

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($url)
            ->setCancelUrl($urlcancel);

        $inputFields = new InputFields();
        $inputFields->setNoShipping(1);

        $webProfile = new WebProfile();
        $webProfile->setName('test' . uniqid())->setInputFields($inputFields);

        $webProfileId = $webProfile->create($apiContext)->getId();

        $payment = new Payment();
        $payment->setExperienceProfileId($webProfileId);
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($apiContext);
        } catch (Exception $ex) {
            echo $ex;
            exit(1);
        }

        return $payment;
    }

    public function executePaymentPP (Request $request) {
        $apiContext = $this->apiContext;

        // true = sandbox, false = live
        $sandbox = $this->sandbox;

        if (!$sandbox) {
            $apiContext->setConfig(
                array(
                  'mode' => 'live',
                )
            );
        }
    
        $paymentId = $request->paymentID;
        $payment = Payment::get($paymentId, $apiContext);
    
        $execution = new PaymentExecution();
        $execution->setPayerId($request->payerID);
    
        try {
            $result = $payment->execute($execution, $apiContext);
        } catch (Exception $ex) {
            echo $ex;
            exit(1);
        }

        return $result;
    }

    public function comprarBoletosAdmin(Request $request)
    {
        try {
            // Boletos disponibles por planta
            $boletosDisponiblesZ1 = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA 1')->count();
            $boletosDisponiblesZ2 = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA 2')->count();
            $disponibles = [$boletosDisponiblesZ1, $boletosDisponiblesZ2];
            if ($request->cantidad > $disponibles[$request->planta-1]) {
                return redirect()->back()->with('error', 'Cantidad de boletos ingresada superior al disponible');
            }
            
            DB::beginTransaction();
            $monto = $request->planta == '1' ? 1600 : 1200;
            // Crear Orden de compra
            $orden = new OrdenCompra();
            $orden->name = $request->nombre;
            $orden->email = $request->email;
            $orden->telefono = $request->telefono;
            $orden->planta = $request->planta;
            $orden->metodoPago = 'ADMINISTRADOR';
            $orden->cantidad = $request->cantidad;
            $orden->monto = $request->cantidad * $monto;
            $orden->status = "En proceso";
            $orden->fechaCompra = date('Y-m-d H:i:s');
            $orden->save();

            // Asignacion de Boletos
            if ($request->cantidad == 1) {
                $boleto = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA ' . $orden->planta)->first();
                $boleto->orden_id = $orden->id;
                $boleto->save();
                $boletosAsignados = $boleto->fila .'-'. $boleto->numero . ' ';
            } else {
                $boletos = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA ' . $orden->planta)->get();
    
                $lastBoletoid = null;
                $boletosAsignados = '';
                $boletosArr = [];
                for ($i = 0; $i < sizeof($boletos); $i++) {
                    $boleto = $boletos[$i];
                    if ($lastBoletoid == null) {
                        $lastBoletoid = $boleto->id;
                        $boleto->orden_id = $orden->id;
                        $boletosArr[] = $boleto;
                        $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                    } else {
                        if ($lastBoletoid == ($boleto->id - 1) && $boleto->numero != 1) {
                            $lastBoletoid = $boleto->id;
                            $boleto->orden_id = $orden->id;
                            $boletosArr[] = $boleto;
                            $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                        } else {
                            $boletosArr = [];

                            $lastBoletoid = $boleto->id;
                            $boleto->orden_id = $orden->id;
                            $boletosArr[] = $boleto;
                            $boletosAsignados = $boleto->fila .'-'. $boleto->numero . ' ';
                        }
                    }

                    if (sizeof($boletosArr) < $request->cantidad) {
                        continue;
                    } else {
                        break;
                    }
                }
                if (sizeof($boletosArr) != $request->cantidad) {
                    DB::rollback();
                    return redirect()->back()->with('error', 'No se dispone la cantidad de boletos con asientos juntos. Intente con una cantidad inferior de boletos');
                }
                foreach ($boletosArr as $boleto) {
                    $boleto->save();
                }
            }
            
            $orden->status = "Completada";
            $orden->save();

            // Envio de email
            $boletosObj = new \stdClass();
            $boletosObj->ordenid = $orden->id;
            $boletosObj->comprador = $orden->name;
            $boletosObj->planta = $orden->planta;
            $boletosObj->asientos = $boletosAsignados;

            $barra = new DNS1D();
            $pdf = PDF::loadView('front.pdf.boletos', compact('orden', 'barra'))->save(storage_path('app\public\boletos-'.$orden->id.'.pdf'));

            Mail::to($orden->email)->send(new BoletosEmail($boletosObj));

            $orden_arr = [
                'email' => $orden->email,
                'ordenid' => $orden->id,
            ];

            DB::commit();
            return redirect()->route('dashboard.boletos')->with('orden', $orden_arr);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function mpNotifications() 
    {
        return response('OK', 201);
    }

    public function reasignacionBoletos(Request $request)
    {
        try {
            $orden = OrdenCompra::find($request->ordenid);
            DB::beginTransaction();

            // Liberar boletos de la orden de compra
            foreach ($orden->boletos as $boleto) {
                $boleto->orden_id = null;
                $boleto->save();
            }

            // Asignar nuevos boletos
            if ($orden->metodoPago == 'ADMINISTRADOR') {
                $boletosAsignados = $this->asignarAdmin($orden);
            } else {
                $boletosAsignados = $this->asignarPublico($orden);
            }

            DB::commit();

            // Send mail
            $boletosObj = new \stdClass();
            $boletosObj->ordenid = $orden->id;
            $boletosObj->comprador = $orden->name;
            $boletosObj->planta = $orden->planta;
            $boletosObj->asientos = $boletosAsignados;

            $barra = new DNS1D();
            $pdf = PDF::loadView('front.pdf.boletos', compact('orden', 'barra'))->save(storage_path('app\public\boletos-'.$orden->id.'.pdf'));

            Mail::to($orden->email)->send(new BoletosEmail($boletosObj));

            return redirect()->route('clientes')->with('success', 'Reasignacion de boletos exitosa');

        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
        
    }

    public function asignarPublico($orden)
    {
        $boletos = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA ' . $orden->planta)->get();

        $lastBoletoid = null;
        $boletosAsignados = '';
        $boletosArr = [];
        $corte = false;

        for ($i = 0; $i < sizeof($boletos); $i++) {

            if ($corte) {
                $lastBoletoid = null;
                $boletosAsignados = '';
                $boletosArr = [];
                $corte = false;
            }

            $boleto = $boletos[$i];
            if ($lastBoletoid == null) {
                $corte = $boleto->proximoCorte;
                $lastBoletoid = $boleto->id;
                $boleto->orden_id = $orden->id;
                $boletosArr[] = $boleto;
                $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
            } else {
                if ($lastBoletoid == ($boleto->id - 1)) {
                    $corte = $boleto->proximoCorte;
                    $lastBoletoid = $boleto->id;
                    $boleto->orden_id = $orden->id;
                    $boletosArr[] = $boleto;
                    $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                } else {
                    $boletosArr = [];

                    $corte = $boleto->proximoCorte;
                    $lastBoletoid = $boleto->id;
                    $boleto->orden_id = $orden->id;
                    $boletosArr[] = $boleto;
                    $boletosAsignados = $boleto->fila .'-'. $boleto->numero . ' ';
                }
            }

            if (sizeof($boletosArr) < $orden->cantidad) {
                continue;
            } else {
                break;
            }
        }

        foreach ($boletosArr as $boleto) {
            $boleto->save();
        }

        return $boletosAsignados;
    }

    public function asignarAdmin($orden)
    {
        $boletosAsignados = '';
        if ($orden->cantidad == 1) {
            $boleto = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA ' . $orden->planta)->first();
            $boleto->orden_id = $orden->id;
            $boleto->save();
            $boletosAsignados = $boleto->fila .'-'. $boleto->numero . ' ';
        } else {
            $boletos = Boleto::where('status', null)->where('orden_id', null)->where('planta', 'ZONA ' . $orden->planta)->get();

            $lastBoletoid = null;
            $boletosAsignados = '';
            $boletosArr = [];
            for ($i = 0; $i < sizeof($boletos); $i++) {
                $boleto = $boletos[$i];
                if ($lastBoletoid == null) {
                    $lastBoletoid = $boleto->id;
                    $boleto->orden_id = $orden->id;
                    $boletosArr[] = $boleto;
                    $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                } else {
                    if ($lastBoletoid == ($boleto->id - 1) && $boleto->numero != 1) {
                        $lastBoletoid = $boleto->id;
                        $boleto->orden_id = $orden->id;
                        $boletosArr[] = $boleto;
                        $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
                    } else {
                        $boletosArr = [];

                        $lastBoletoid = $boleto->id;
                        $boleto->orden_id = $orden->id;
                        $boletosArr[] = $boleto;
                        $boletosAsignados = $boleto->fila .'-'. $boleto->numero . ' ';
                    }
                }

                if (sizeof($boletosArr) < $orden->cantidad) {
                    continue;
                } else {
                    break;
                }
            }
            if (sizeof($boletosArr) != $orden->cantidad) {
                DB::rollback();
                return redirect()->back()->with('error', 'No se dispone la cantidad de boletos con asientos juntos. Intente con una cantidad inferior de boletos');
            }
            foreach ($boletosArr as $boleto) {
                $boleto->save();
            }
        }

        return $boletosAsignados;
    }

    public function bloquearBoleto(Request $request)
    {
        $boleto = Boleto::where('orden_id', null)
                        ->where('planta', $request->zona)
                        ->where('fila', $request->fila)
                        ->where('numero', $request->numero)
                        ->first();
        if (isset($boleto)) {
            $boleto->status = 'bloqueado';
            $boleto->save();
        }
        return redirect()->route('dashboard.boletos.grafico');
    }

    public function desbloquearBoleto(Request $request)
    {
        $boleto = Boleto::where('orden_id', null)
                        ->where('planta', $request->zona)
                        ->where('fila', $request->fila)
                        ->where('numero', $request->numero)
                        ->first();
        if (isset($boleto)) {
            $boleto->status = null;
            $boleto->save();
        }
        return redirect()->route('dashboard.boletos.grafico');
    }

    public function comprarBoletosSeleccionados(Request $request)
    {
        $formData = json_decode($request->formData);
        $boletosid = explode(',', $request->boletos);

        // Verificar Boletos Seleccionados esten disponibles
        $boletos = Boleto::whereIn('id', $boletosid)->where('orden_id', null)->get();

        if (count($boletos) != $formData->cantidad) {
            return redirect()->route('dashboard.boletos')->with('error', 'Boletos seleccionado no disponibles');
        }

        try {
            DB::beginTransaction();
            $monto = 1600;
            // Crear Orden de compra
            $orden = new OrdenCompra();
            $orden->name = $formData->nombre;
            $orden->email = $formData->email;
            $orden->telefono = $formData->telefono;
            $orden->planta = $formData->planta;
            $orden->metodoPago = 'ADMINISTRADOR';
            $orden->cantidad = $formData->cantidad;
            $orden->monto = $formData->cantidad * $monto;
            $orden->status = "En proceso";
            $orden->fechaCompra = date('Y-m-d H:i:s');
            $orden->save();

            // Asignacion de Boletos
            $boletosAsignados = '';
            foreach ($boletos as $boleto) {
                $boleto->orden_id = $orden->id;
                $boleto->save();
                $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
            }
            
            $orden->status = "Completada";
            $orden->save();

            // Envio de email
            $boletosObj = new \stdClass();
            $boletosObj->ordenid = $orden->id;
            $boletosObj->comprador = $orden->name;
            $boletosObj->planta = $orden->planta;
            $boletosObj->asientos = $boletosAsignados;

            $barra = new DNS1D();
            $pdf = PDF::loadView('front.pdf.boletos', compact('orden', 'barra'))->save(storage_path('app\public\boletos-'.$orden->id.'.pdf'));

            Mail::to($orden->email)->send(new BoletosEmail($boletosObj));

            $orden_arr = [
                'email' => $orden->email,
                'ordenid' => $orden->id,
            ];

            DB::commit();
            return redirect()->route('dashboard.boletos')->with('orden', $orden_arr);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function showbar($codigo) {
        
        $n = substr_count($codigo, '789');

        if ( $n == 1) {
            $datos = explode('789',$codigo);
            $ordenid = intval(substr($datos[1], 0, strlen($datos[1]) - 1));
            $boletoid = intval($datos[0]);
            $cb = $this->generateCB($boletoid, $ordenid);
            return $this->getData($boletoid, $ordenid, $codigo, $cb);
        } else {
            if ( $n == 2) {
                $datos = explode('789',$codigo);
                $ordenid = intval(substr($datos[2], 0, strlen($datos[2]) - 1));
                $boletoid = intval($datos[0] . '789' . $datos[1]);
                $cb = $this->generateCB($boletoid, $ordenid);
                return $this->getData($boletoid, $ordenid, $codigo, $cb);
            } else {
                return "Código de barra inexistente";
            }
        }
    }

    private function generateCB($boletoid, $ordenid) {

        $barra = new DNS1D();
        $bcode = $barra::getBarcodeSVG($boletoid . "789" . $ordenid, "EAN13", 1.3, 25);
        preg_match("/<desc>(.*?)<\\/desc>/si", $bcode, $match);
        return $match[1]; 
    }

    private function getData($boletoid, $ordenid, $codigo, $cb) {

        if ((int)$cb == (int)$codigo) {

            $boleto = Boleto::find($boletoid);
            $orden = OrdenCompra::find($ordenid);

            if ($boleto->planta == 2) {
                $zona = 'ZONA 2';  
            } else {
                $zona = 'ZONA 1';
            }

            $asiento = $boleto->numero;
            $fila = $boleto->fila;

            $piso = 'P1';
            if ($boleto->id >= 342 && $boleto->id <= 785) {
                $piso = 'P2';
            }

            return $data = [
                // 'orden' => $orden,
                // 'boleto' => $boleto,
                'zona' => $zona,
                'piso' => $piso,
                'asiento' => $asiento,
                'fila' => $fila,
                'nombre' => $orden->name,
                'email' => $orden->email,
                'telefono' => $orden->telefono,
            ];
        } else {
            return "Código de barra inexistente";
        }
    }
}
