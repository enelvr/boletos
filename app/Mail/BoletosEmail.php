<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BoletosEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $boletos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($boletos)
    {
        $this->boletos = $boletos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('hatzalah@789.com.mx')
                    ->view('mails.boletos')
                    ->attach(storage_path('app\public\boletos-'.$this->boletos->ordenid.'.pdf'));
    }
}
