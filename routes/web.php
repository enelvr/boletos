<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Boleto;
use App\Models\OrdenCompra;
use \Milon\Barcode\DNS1D;
Route::get('/', function () {
    return view('front.page.home.index');
})->name('home');

Route::get('/boletos', 'BoletosController@boletosBuy')->name('boletos');

Route::post('/comprar-boletos', 'BoletosController@comprar')->name('compraBoletos');
Route::get('/asignar-boletos/{orden}', 'BoletosController@asignarBoletos')->name('boletos.asignar');
Route::get('/compra-finalizada', 'BoletosController@compraFin')->name('compra.fin');
// Route::get('/download-boletos/{orden}', 'BoletosController@downloadBoletos')->name('downloadBoletos');
Route::post('/download-boletos', 'BoletosController@downloadBoletos')->name('downloadBoletos');

		
Auth::routes(['register' => false]);

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard/clientes', 'DashboardController@clientes')->name('clientes');

Route::get('/dashboard/boletos', 'DashboardController@boletos')->name('dashboard.boletos');
Route::post('/dashboard/boletos/comprar', 'BoletosController@comprarBoletosAdmin')->name('dashboard.boletos.comprar');

Route::post('/dashboard/boletos/reasignar', 'BoletosController@reasignacionBoletos')->name('dashboard.boletos.reasignar');

Route::get('/dashboard/boletos/grafico', 'DashboardController@boletosGrafico')->name('dashboard.boletos.grafico');
Route::post('/dashboard/boletos/bloquear', 'BoletosController@bloquearBoleto')->name('dashboard.boletos.bloquear');
Route::post('/dashboard/boletos/desbloquear', 'BoletosController@desbloquearBoleto')->name('dashboard.boletos.desbloquear');

Route::post('/dashboard/boletos/seleccionar', 'DashboardController@selectorBoletos')->name('dashboard.boletos.seleccionar');
Route::post('/dashboard/boletos/comprar/seleccionados', 'BoletosController@comprarBoletosSeleccionados')->name('dashboard.boletos.comprar.seleccionados');

Route::get('/mp/notifications', 'BoletosController@mpNotifications')->name('notifications');

Route::get('/codigo/{codigo}', 'BoletosController@showbar')->name('showbar');
