<?php

use Illuminate\Database\Seeder;
use App\Models\Boleto;

class BoletosZ0Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filasZ0 = [
            ['fila' => 'AA', 'asientos' => 24, 'cortes' => [8, 16, 24]],
            ['fila' => 'A', 'asientos' => 27, 'cortes' => [9, 18, 27]],
            ['fila' => 'B', 'asientos' => 27, 'cortes' => [9, 18, 27]],
            ['fila' => 'C', 'asientos' => 28, 'cortes' => [9, 19, 28]],
            ['fila' => 'D', 'asientos' => 10, 'cortes' => [10]],
            ['fila' => 'E', 'asientos' => 29, 'cortes' => [9, 20, 29]],
            ['fila' => 'F', 'asientos' => 29, 'cortes' => [9, 20, 29]],
            ['fila' => 'G', 'asientos' => 30, 'cortes' => [9, 21, 30]],
            ['fila' => 'H', 'asientos' => 30, 'cortes' => [9, 21, 30]],
            ['fila' => 'I', 'asientos' => 31, 'cortes' => [9, 22, 31]],
            ['fila' => 'J', 'asientos' => 31, 'cortes' => [9, 22, 31]],
            ['fila' => 'K', 'asientos' => 4, 'cortes' => []],
        ];

        for ($i = 0; $i < sizeof($filasZ0); $i++) {
            for($j = 0; $j < $filasZ0[$i]['asientos']; $j++) {
                $corte = false;
                if(in_array($j + 1, $filasZ0[$i]['cortes'])) {
                    $corte = true;
                }
                Boleto::create([
                    'orden_id' => null,
                    'planta' => 'ZONA 0',
                    'fila' => $filasZ0[$i]['fila'],
                    'numero' => $j + 1,
                    'fecha' => '2019-09-22 00:00:00',
                    'proximoCorte' => $corte,
                    'status' => null,
                ]);
            }
        }
    }
}
