<?php

use App\Models\Boleto;
use Illuminate\Database\Seeder;

class DeleteBoletosDuplicadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filasZ1 = [
            ['fila' => 'K', 'asientos' => 32,],
            ['fila' => 'L', 'asientos' => 32,],
            ['fila' => 'M', 'asientos' => 31,],
            ['fila' => 'N', 'asientos' => 31,],
            ['fila' => 'O', 'asientos' => 32,],
            ['fila' => 'P', 'asientos' => 32,],
            ['fila' => 'Q', 'asientos' => 31,],
            ['fila' => 'R', 'asientos' => 31,],
            ['fila' => 'S', 'asientos' => 32,],
            ['fila' => 'T', 'asientos' => 31,],
            ['fila' => 'U', 'asientos' => 17,],
            ['fila' => 'V', 'asientos' => 13,],
            ['fila' => 'A', 'asientos' => 4,],
            ['fila' => 'B', 'asientos' => 4,],
            ['fila' => 'C', 'asientos' => 4,],
            ['fila' => 'D', 'asientos' => 4,],
            ['fila' => 'E', 'asientos' => 33,],
            ['fila' => 'F', 'asientos' => 33,],
        ];

        $filasZ2 = [
            ['fila' => 'G', 'asientos' => 33,],
            ['fila' => 'H', 'asientos' => 21,],
            ['fila' => 'I', 'asientos' => 22,],
            ['fila' => 'J', 'asientos' => 22,],
            ['fila' => 'K', 'asientos' => 22,],
            ['fila' => 'L', 'asientos' => 22,],
            ['fila' => 'M', 'asientos' => 26,],
            ['fila' => 'N', 'asientos' => 34,],
            ['fila' => 'O', 'asientos' => 33,],
            ['fila' => 'P', 'asientos' => 33,],
            ['fila' => 'Q', 'asientos' => 34,],
            ['fila' => 'R', 'asientos' => 34,],
            ['fila' => 'S', 'asientos' => 26,],
        ];

        $ini = 4;
        for ($i = 0; $i < sizeof($filasZ1); $i++) {
            for($j = $ini; $j < $filasZ1[$i]['asientos']; $j++) {
                $fila = $filasZ1[$i]['fila'];
                $numero = $j + 1;
                $zona = 'ZONA 1';

                $boleto = Boleto::where('fila', $fila)
                    ->where('numero', $numero)
                    ->where('planta', $zona)
                    ->latest()->first();

                $boleto->delete();
            }
            $ini = 0;
        }

        for ($i = 0; $i < sizeof($filasZ2); $i++) {
            for($j = 0; $j < $filasZ2[$i]['asientos']; $j++) {
                $fila = $filasZ2[$i]['fila'];
                $numero = $j + 1;
                $zona = 'ZONA 2';

                $boleto = Boleto::where('fila', $fila)
                    ->where('numero', $numero)
                    ->where('planta', $zona)
                    ->latest()->first();

                $boleto->delete();
            }
        }
    }
}
