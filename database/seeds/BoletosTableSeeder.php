<?php

use Illuminate\Database\Seeder;
use App\Models\Boleto;

class BoletosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filasZ1 = [
            // ['fila' => 'I', 'asientos' => 31, 'cortes' => [22, 31]],
            // ['fila' => 'J', 'asientos' => 31, 'cortes' => [9, 22, 31]],
            ['fila' => 'K', 'asientos' => 32, 'cortes' => [9, 23, 32]],
            ['fila' => 'L', 'asientos' => 32, 'cortes' => [9, 23, 32]],
            ['fila' => 'M', 'asientos' => 31, 'cortes' => [8, 23, 31]],
            ['fila' => 'N', 'asientos' => 31, 'cortes' => [8, 23, 31]],
            ['fila' => 'O', 'asientos' => 32, 'cortes' => [8, 24, 32]],
            ['fila' => 'P', 'asientos' => 32, 'cortes' => [8, 24, 32]],
            ['fila' => 'Q', 'asientos' => 31, 'cortes' => [7, 24, 31]],
            ['fila' => 'R', 'asientos' => 31, 'cortes' => [7, 24, 31]],
            ['fila' => 'S', 'asientos' => 32, 'cortes' => [7, 25, 32]],
            ['fila' => 'T', 'asientos' => 31, 'cortes' => [6, 25, 31]],
            ['fila' => 'U', 'asientos' => 17, 'cortes' => []],
            ['fila' => 'V', 'asientos' => 13, 'cortes' => []],
            ['fila' => 'A', 'asientos' => 4, 'cortes' => [2, 4]],
            ['fila' => 'B', 'asientos' => 4, 'cortes' => [2, 4]],
            ['fila' => 'C', 'asientos' => 4, 'cortes' => [2, 4]],
            ['fila' => 'D', 'asientos' => 4, 'cortes' => [2, 4]],
            ['fila' => 'E', 'asientos' => 33, 'cortes' => [6, 27, 33]],
            ['fila' => 'F', 'asientos' => 33, 'cortes' => [6, 27, 33]],
        ];

        $filasZ2 = [
            ['fila' => 'G', 'asientos' => 33, 'cortes' => [6, 27, 33]],
            ['fila' => 'H', 'asientos' => 21, 'cortes' => [6, 15, 21]],
            ['fila' => 'I', 'asientos' => 22, 'cortes' => [6, 17, 22]],
            ['fila' => 'J', 'asientos' => 22, 'cortes' => [6, 17, 22]],
            ['fila' => 'K', 'asientos' => 22, 'cortes' => [5, 17, 22]],
            ['fila' => 'L', 'asientos' => 22, 'cortes' => [5, 17, 22]],
            ['fila' => 'M', 'asientos' => 26, 'cortes' => [7, 20, 26]],
            ['fila' => 'N', 'asientos' => 34, 'cortes' => [11, 24, 34]],
            ['fila' => 'O', 'asientos' => 33, 'cortes' => [10, 24, 33]],
            ['fila' => 'P', 'asientos' => 33, 'cortes' => [10, 24, 33]],
            ['fila' => 'Q', 'asientos' => 34, 'cortes' => [10, 25, 34]],
            ['fila' => 'R', 'asientos' => 34, 'cortes' => [10, 25, 34]],
            ['fila' => 'S', 'asientos' => 26, 'cortes' => [13, 26]],
        ];

        $ini = 4;
        for ($i = 0; $i < sizeof($filasZ1); $i++) {
            for($j = $ini; $j < $filasZ1[$i]['asientos']; $j++) {
                $corte = false;
                if(in_array($j + 1, $filasZ1[$i]['cortes'])) {
                    $corte = true;
                }
                Boleto::create([
                    'orden_id' => null,
                    'planta' => 'ZONA 1',
                    'fila' => $filasZ1[$i]['fila'],
                    'numero' => $j + 1,
                    'fecha' => '2019-09-22 00:00:00',
                    'proximoCorte' => $corte,
                ]);
            }
            $ini = 0;
        }

        for ($i = 0; $i < sizeof($filasZ2); $i++) {
            for($j = 0; $j < $filasZ2[$i]['asientos']; $j++) {
                $corte = false;
                if(in_array($j + 1, $filasZ2[$i]['cortes'])) {
                    $corte = true;
                }
                Boleto::create([
                    'orden_id' => null,
                    'planta' => 'ZONA 2',
                    'fila' => $filasZ2[$i]['fila'],
                    'numero' => $j + 1,
                    'fecha' => '2019-09-22 00:00:00',
                    'proximoCorte' => $corte,
                ]);
            }
        }
    }
}
