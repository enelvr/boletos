<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoletosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('orden_id')->nullable();
            $table->string('planta', 10);
            $table->string('fila', 10);
            $table->integer('numero');
            $table->datetime('fecha');
            $table->boolean('proximoCorte');
            $table->timestamps();

            $table->foreign('orden_id')->references('id')->on('ordenes_compra');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletos');
    }
}
