@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        @include('admin.dashboard.includes.alerts')
      <h2>Listado de clientes</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <table id="dataTable" class="display nowrap table table-bordered table-striped dataTable">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Correo Electronico</th>
            <th>Telefono</th>
            <th>Cant</th>
            <th>Zona</th>
            <th>Boletos Asignados</th>
            <th>Comprobante de Pago</th>
            <th>Hora</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($compras->sortByDesc('created_at') as $compra)
          <tr>
            <td>{{ $compra->id }}</td>
            <td>{{ $compra->name }}</td>
            <td>{{ $compra->email }}</td>
            <td>{{ $compra->telefono }}</td>
            <td>{{ $compra->cantidad }}</td>
            <td>
              @if ($compra->planta == 0)
                1 VIP
              @else
                {{ $compra->planta }}
              @endif
            </td>
            <?php 
              $boletosAsignados = '';
              foreach ($compra->boletos as $boleto) {
                $boletosAsignados .= $boleto->fila .'-'. $boleto->numero . ' ';
              }
            ?>
            <td>{{ $boletosAsignados }}</td>
            <td>{{ $compra->metodoPago .' : '. $compra->transactionID }}</td>
            <td>{{ $compra->created_at }}</td>
            <td>
              <form action="{{ route('downloadBoletos') }}" method="POST">
                @csrf
                <input type="hidden" name="ordenid" value="{{ $compra->id }}">
                <button type="submit" class="btn-circle btn-sm btn btn-primary">
                  <i class="fas fa-file-download"></i>
                </button>
              </form>

              @if ($compra->planta != 0)
              <form action="{{ route('dashboard.boletos.reasignar') }}" method="POST">
                @csrf
                <input type="hidden" name="ordenid" value="{{ $compra->id }}">
                <button type="submit" class="btn-circle btn-sm btn btn-warning btnSubmit">
                  <i class="fas fa-redo"></i>
                </button>
              </form>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" type="text/javascript"></script>
<script>
  $(document).ready( function () {
      $('#dataTable').DataTable({
        "order": [[ 8, "desc" ]],
        dom: 'Bfrtip',
        buttons: ['excel']
      });

      $('.btnSubmit').click(function(e) {
        e.preventDefault();
        const r = confirm('Desea reasignar boletos?');
        if (r) {
          $(this).closest('form').submit();
        }
      });  
  });
</script>
@endsection