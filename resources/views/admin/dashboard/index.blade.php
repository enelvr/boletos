@extends('admin.layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-12">
      <h2>Dashboard</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-3">
      <div class="card border-left-primary shadow">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                Boletos Totales
              </div>
              <div class="h6 mb-0 font-weight-bold text-gray-800">
                Zona 1 VIP: {{ $boletosZ0 }} <br>
                Zona 1: {{ $boletosZ1 }} <br>
                Zona 2: {{ $boletosZ2 }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card border-left-warning shadow">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                Boletos Vendidos
              </div>
              <div class="h6 mb-0 font-weight-bold text-gray-800">
                Zona 1 VIP: {{ $boletosVendidosZ0 }} <br>
                Zona 1: {{ $boletosVendidosZ1 }} <br>
                Zona 2: {{ $boletosVendidosZ2 }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card border-left-info shadow">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                Boletos Disponibles
              </div>
              <div class="h6 mb-0 font-weight-bold text-gray-800">
                Zona 1 VIP: {{ $boletosDisponiblesZ0 }} <br>
                Zona 1: {{ $boletosDisponiblesZ1 }} <br>
                Zona 2: {{ $boletosDisponiblesZ2 }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card border-left-danger shadow">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                Boletos Bloqueados
              </div>
              <div class="h6 mb-0 font-weight-bold text-gray-800">
                Zona 1: {{ $boletosBloqueadosZ1 }} <br>
                Zona 2: {{ $boletosBloqueadosZ2 }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-3">
      <div class="card border-left-success shadow">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                Monto Recaudado
              </div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                ${{ number_format($montoRecaudado, 2) }}
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection