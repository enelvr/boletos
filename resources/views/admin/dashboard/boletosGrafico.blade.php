@extends('admin.layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-12">
      <h2>Boletos</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-zona1-tab" data-toggle="pill" href="#pills-zona1" role="tab" aria-controls="pills-zona1" aria-selected="true">Zona 1</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-zona2-tab" data-toggle="pill" href="#pills-zona2" role="tab" aria-controls="pills-zona2" aria-selected="true">Zona 2</a>
          </li>
        </ul>
    </div>
  </div>
  <div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-zona1" role="tabpanel" aria-labelledby="pills-zona1-tab">
      {{-- Zona 1 --}}
      <div class="row">
        <div class="col-lg-12" style="overflow-x:auto;">
          <table id="dataTable" class="table table-bordered dataTable" style="font-size: 10px;">
            <tbody>
              @foreach ($filas1 as $fila)
                  <tr>
                    <td>{{ $fila['fila'] }}</td>
                    @foreach ($fila['boletos'] as $boleto)
                      @if ($boleto['numero'] == 0)
                        <td></td>
                      @elseif ($boleto['bloqueado'] == 'bloqueado')
                        <td style="background-color: #d8d8d8" data-toggle="tooltip" data-placement="top" title="No disponible">{{ $boleto['numero'] }}</td>
                      @elseif ($boleto['bloqueado'] == 'bloqueado2')
                        <td class="bloqueado">
                          <a href="#" data-toggle="modal" data-target="#bloqueadoModal" data-fila="{{ $fila['fila'] }}" data-numero="{{ $boleto['numero'] }}" data-zona="ZONA 1">
                            <span data-toggle="tooltip" data-placement="top" title="Bloqueado">
                              {{ $boleto['numero'] }}
                            </span>
                          </a>
                        </td>
                      @elseif ($boleto['bloqueado'] == 'vendido')
                        <td class="vendidos">
                          <a href="#" data-toggle="modal" data-target="#vendidoModal" data-orden="{{ $boleto['orden'] }}" data-boleto="Zona 1 {{ $fila['fila'] . '-' . $boleto['numero'] }}">
                            <span data-toggle="tooltip" data-placement="top" title="Vendido">
                              {{ $boleto['numero'] }}
                            </span>
                          </a>
                        </td>
                      @elseif ($boleto['bloqueado'] == 'disponible')
                        <td class="disponibles">
                          <a href="#" data-toggle="modal" data-target="#disponibleModal" data-fila="{{ $fila['fila'] }}" data-numero="{{ $boleto['numero'] }}" data-zona="ZONA 1">
                            <span data-toggle="tooltip" data-placement="top" title="Disponible">
                              {{ $boleto['numero'] }}
                            </span>
                          </a>
                        </td>
                      @endif
                    @endforeach
                    <td>{{ $fila['fila'] }}</td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="pills-zona2" role="tabpanel" aria-labelledby="pills-zona2-tab">
      {{-- Zona 2 --}}
      <div class="row">
        <div class="col-lg-12" style="overflow-x:auto;">
          <table id="dataTable2" class="table table-bordered dataTable" style="font-size: 10px;">
            <tbody>
              @foreach ($filas2 as $fila)
                  <tr>
                    <td>{{ $fila['fila'] }}</td>
                    @foreach ($fila['boletos'] as $boleto)
                      @if ($boleto['numero'] == 0)
                        <td></td>
                      @elseif ($boleto['bloqueado'] == 'bloqueado')
                        <td style="background-color: #d8d8d8" data-toggle="tooltip" data-placement="top" title="No disponible">{{ $boleto['numero'] }}</td>
                      @elseif ($boleto['bloqueado'] == 'bloqueado2')
                        <td class="bloqueado">
                          <a href="#" data-toggle="modal" data-target="#bloqueadoModal" data-fila="{{ $fila['fila'] }}" data-numero="{{ $boleto['numero'] }}" data-zona="ZONA 2">
                            <span data-toggle="tooltip" data-placement="top" title="Bloqueado">
                              {{ $boleto['numero'] }}
                            </span>
                          </a>
                        </td>
                      @elseif ($boleto['bloqueado'] == 'vendido')
                        <td class="vendidos">
                          <a href="#" data-toggle="modal" data-target="#vendidoModal" data-orden="{{ $boleto['orden'] }}" data-boleto="Zona 2 {{ $fila['fila'] . '-' . $boleto['numero'] }}">
                            <span data-toggle="tooltip" data-placement="top" title="Vendido">
                              {{ $boleto['numero'] }}
                            </span>
                          </a>
                        </td>
                      @elseif ($boleto['bloqueado'] == 'disponible')
                        <td class="disponibles">
                          <a href="#" data-toggle="modal" data-target="#disponibleModal" data-fila="{{ $fila['fila'] }}" data-numero="{{ $boleto['numero'] }}" data-zona="ZONA 2">
                            <span data-toggle="tooltip" data-placement="top" title="Disponible">
                              {{ $boleto['numero'] }}
                            </span>
                          </a>
                        </td>
                      @endif
                    @endforeach
                    <td>{{ $fila['fila'] }}</td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  @include('admin.dashboard.includes.modalBloqueado')
  @include('admin.dashboard.includes.modalDisponible')
  @include('admin.dashboard.includes.modalVendido')
@endsection

@section('scripts')
<script>
  $(document).ready(function(){$('[data-toggle="tooltip"]').tooltip();$('#vendidoModal').on('show.bs.modal',function(event){var button=$(event.relatedTarget);var orden=button.data('orden');var boleto=button.data('boleto');var modal=$(this);modal.find('.boleto').text(boleto);modal.find('.ordencompra').text(orden.id);modal.find('.nombrecliente').text(orden.nombre);modal.find('.emailcliente').text(orden.email);modal.find('.tlfcliente').text(orden.telefono);modal.find('.cantidadcliente').text(orden.cantidad);modal.find('.boletosasignadoscliente').text(orden.asignados);modal.find('.horacliente').text(orden.hora)});$('#disponibleModal').on('show.bs.modal',function(event){var button=$(event.relatedTarget);var fila=button.data('fila');var numero=button.data('numero');var zona=button.data('zona');var boleto=zona+' '+fila+'-'+numero;var modal=$(this);modal.find('.boleto').text(boleto);modal.find('input[name="zona"]').val(zona);modal.find('input[name="fila"]').val(fila);modal.find('input[name="numero"]').val(numero)});$('#bloqueadoModal').on('show.bs.modal',function(event){var button=$(event.relatedTarget);var fila=button.data('fila');var numero=button.data('numero');var zona=button.data('zona');var boleto=zona+' '+fila+'-'+numero;var modal=$(this);modal.find('.boleto').text(boleto);modal.find('input[name="zona"]').val(zona);modal.find('input[name="fila"]').val(fila);modal.find('input[name="numero"]').val(numero)})})
</script>
@endsection

@section('styles')
  <style>
    .bloqueado{padding:0px!important;background-color:#e74a3b}.bloqueado a{text-decoration:none;color:white;display:block;width:100%;padding:8px 10px}.bloqueado a:hover{text-decoration:none}.vendidos{padding:0px!important;background-color:#1cc88a}.vendidos a{text-decoration:none;color:white;display:block;width:100%;padding:8px 10px}.vendidos a:hover{text-decoration:none}.disponibles{padding:0px!important;background-color:#36b9cc}.disponibles a{text-decoration:none;color:black;display:block;width:100%;padding:8px 10px}.disponibles a:hover{text-decoration:none}
  </style>
@endsection