@extends('admin.layouts.app')
@section('content')
  <div class="row">
    <div class="col-lg-10">
      <h2>Selector de Boletos</h2>
    </div>
    <div class="col-lg-2">
      <form action="{{ route('dashboard.boletos.comprar.seleccionados') }}" method="post" id="form-boletos">
        @csrf
        <input type="hidden" name="boletos">
        <input type="hidden" name="formData" value="{{ json_encode($formData) }}">
        <button type="button" class="btn btn-primary" id="btnSubmit" onclick="submitBoletos()" disabled>Continuar</button>
      </form>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12" style="overflow-x:auto;">
      <table id="dataTable" class="table table-bordered dataTable" style="font-size: 10px;">
        <tbody>
          @foreach ($filas0 as $fila)
              <tr>
                <td>{{ $fila['fila'] }}</td>
                @foreach ($fila['boletos'] as $boleto)
                  @if ($boleto['numero'] == 0)
                    <td></td>
                  @elseif ($boleto['bloqueado'] == 'vendido')
                    <td class="vendidos">
                      <a href="#">
                        <span data-toggle="tooltip" data-placement="top" title="No Disponible">
                          {{ $boleto['numero'] }}
                        </span>
                      </a>
                    </td>
                  @elseif ($boleto['bloqueado'] == 'disponible')
                    <td class="disponibles">
                      <a href="#" onclick="selectBoleto({{ $boleto['id'] }}, this)">
                        <span>
                          {{ $boleto['numero'] }}
                        </span>
                      </a>
                    </td>
                  @endif
                @endforeach
                <td>{{ $fila['fila'] }}</td>
              </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  $(document).ready(function(){$('[data-toggle="tooltip"]').tooltip()});let boletos=[];let cantidad='{{ $formData["cantidad"] }}'
console.log(cantidad);function selectBoleto(boletoid,element){let index=boletos.indexOf(boletoid);let td=$(element).parent();if(index==-1&&boletos.length<cantidad){td.addClass('seleccionado');boletos.push(boletoid)}else if(index!=-1){td.removeClass('seleccionado');boletos.splice(index,1)}
if(boletos.length==cantidad){$('#btnSubmit').attr('disabled',!1)}else{$('#btnSubmit').attr('disabled',!0)}
console.log(boletos)}
function submitBoletos(){$('input[name=boletos]').val(boletos);$('#form-boletos').submit()}
</script>
@endsection

@section('styles')
  <style>
    .bloqueado{padding:0px!important;background-color:#e74a3b}.bloqueado a{text-decoration:none;color:white;display:block;width:100%;padding:8px 10px}.bloqueado a:hover{text-decoration:none}.vendidos{padding:0px!important;background-color:#1cc88a}.vendidos a{text-decoration:none;color:white;display:block;width:100%;padding:8px 10px}.vendidos a:hover{text-decoration:none}.seleccionado{background-color:gray!important}.disponibles{padding:0px!important;background-color:#36b9cc}.disponibles a{text-decoration:none;color:black;display:block;width:100%;padding:8px 10px}.disponibles a:hover{text-decoration:none}
  </style>
@endsection