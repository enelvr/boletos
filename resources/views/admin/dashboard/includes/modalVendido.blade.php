<!-- Modal -->
<div class="modal fade" id="vendidoModal" tabindex="-1" role="dialog" aria-labelledby="vendidoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="vendidoModalLabel">Información boleto <span class="boleto"></span></h5>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12"><p>Orden de Compra: <span class="ordencompra"></span></p></div>
            <div class="col-sm-12"><p>Nombre: <span class="nombrecliente"></span></p></div>
            <div class="col-sm-12"><p>Correo Electronico: <span class="emailcliente"></span></p></div>
            <div class="col-sm-12"><p>Telefono: <span class="tlfcliente"></span></p></div>
            <div class="col-sm-12"><p>Cantidad de Boletos: <span class="cantidadcliente"></span></p></div>
            <div class="col-sm-12"><p>Boletos Asignados: <span class="boletosasignadoscliente"></span></p></div>
            <div class="col-sm-12"><p>Hora de Compra: <span class="horacliente"></span></p></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>