@if (session('orden'))
<?php $orden = session('orden'); ?>
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    ¡Boletos comprados exitosamente! Se envio un correo electronico a {{ $orden['email'] }} con los boletos.

    <form action="{{ route('downloadBoletos') }}" method="POST">
    @csrf
        <input type="hidden" name="ordenid" value="{{ $orden['ordenid'] }}">
        Haga <strong><button type="submit" class="btnLink">click aquí</button></strong> para descargar los boletos.
    </form> 

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if (session('error'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('error') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if (session('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif