<!-- Modal -->
<div class="modal fade" id="bloqueadoModal" tabindex="-1" role="dialog" aria-labelledby="bloqueadoModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="bloqueadoModalLabel">Desbloquear boleto <span class="boleto"></span></h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <form action="{{ route('dashboard.boletos.desbloquear') }}" method="POST">
          @csrf
          <input type="hidden" name="zona" value="">
          <input type="hidden" name="fila" value="">
          <input type="hidden" name="numero" value="">
          <button type="submit" class="btn btn-warning">Desbloquear</button>
        </form>
      </div>
    </div>
  </div>
</div>