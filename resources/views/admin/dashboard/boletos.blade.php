@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">        
        @include('admin.dashboard.includes.alerts')
        <h2>Compra de Boletos Administrador</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
        <form action="{{ route('dashboard.boletos.comprar') }}" method="POST" id="compra-boletos-admin">
        @csrf
            <div class="row">
                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                        <label for="zonaSelect">ZONA</label>
                        <select class="form-control" id="zonaSelect" name="planta">
                            @if($disponibles2[0] != 0)
                            <option value="1">Zona 1 - $1,600 MXN</option>
                            @endif
                            @if($disponibles2[1] != 0)
                            <option value="2">Zona 2 - $1,200 MXN</option>
                            @endif
                            <option value="0">Zona 1 VIP</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label for="nombre" class="form-label">NOMBRE</label>
                        <input type="text" name="nombre" class="form-control form-input" placeholder="Nombre Completo" required>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label for="email" class="form-label">CORREO ELECTRÓNICO</label>
                        <input type="email" name="email" class="form-control form-input" placeholder="ejemplo@gmail.com" required>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label for="telefono" class="form-label">TELÉfONO</label>
                        <input type="text" name="telefono" class="form-control form-input" placeholder="55 555 555" required>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label for="cantidad" class="form-label">CANTIDAD</label>
                        <input type="number" name="cantidad" class="form-control form-input" placeholder="0" value="1" min="1" step="1" required>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="comprarBtn">Comprar</button>
        </form>
    </div>
  </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){function updateMaxCant(){const selected=$('#zonaSelect option:selected').val();let url='{{ route("dashboard.boletos.comprar") }}';let text='Comprar'
if(selected==0){url='{{ route("dashboard.boletos.seleccionar") }}';text='Seleccionar Boletos'}
$('#compra-boletos-admin').attr('action',url);$('#comprarBtn').text(text);const disponible=disponibles[selected-1];$('input[name="cantidad"]').attr('max',disponible)}
const disponibles=JSON.parse('{!! $disponibles !!}');updateMaxCant();$('#zonaSelect').change(updateMaxCant)})
</script>
@endsection

@section('styles')
<style>
    .btnLink{background:none;margin:0;padding:0;border:none;cursor:pointer;color:#2aa0b3}.btnLink:hover{text-decoration:underline}
</style>    
@endsection