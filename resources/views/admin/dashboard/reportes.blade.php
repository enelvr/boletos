@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
      <h2>Estatus de los Boletos</h2>
    </div>
</div>
<hr>
<div class="row">
	<div class="col-lg-12">
		<h3>Boletos Vendidos</h3>
		
		@foreach($boletosVendidos as $bv)
				<div class="card badge border-left-success">
					<a class="btn btn-danger btn-circle btn-sm">{{ $bv->id}}</a> <br>
					@foreach($bv->boletos as $b)
						<span class="badge badge-success">{{ $b->fila. '-' . $b->numero }}</span>
					@endforeach
				</div>
		@endforeach

	</div>

	
</div>
<hr>
<div class="row">
	<div class="col-lg-12">
		<h3>Ordenes y Boletos En Proceso</h3>
		
		@foreach($boletosProceso as $bp)
				<div class="card badge border-left-warning">
					{{ $bp->id}} <br>
					@foreach($bp->boletos as $b)
						<span class="badge badge-warning">{{ $b->fila. '-' . $b->numero }}</span>
					@endforeach
				</div>
		@endforeach

	</div>
</div>
@endsection