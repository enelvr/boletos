<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

    <!-- Css-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('page/css/hatzalah.css') }}">
</head>
<body id="page-top">
    <!-- Navigation -->
  <a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="/#page-top">Inicio</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="/#lior-suchard">Lior Suchard
</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="/#location">Ubicación</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="/#about">Acerca De</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="/#tickets">Boletos</a>
      </li>
      <li class="sidebar-nav-item margen2">
        <a>DÓNDE</a>
        <p class="font3 font4 margen6">AUDITORIO RAFAEL</p>
        <p class="font3 font4 margen6">Y REGINA KALACH</p>
        <p class="font3 font4 margen6">Colegio Hebreo Monte Sinaí</p>
      </li>
    </ul>
  </nav>

    @yield('content')

    <div class="divider"></div>
  <!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row">
          <div class="col-md-6 text-center">
            <p>Necesitas Apoyo para la compra de boletos</p>
            <h3>llama Ya! a 789</h3>
            <p><i class="fa fa-phone"></i> 913 17 416</p>
          </div>
          <div class="col-md-6 text-center">
            <p>Conoce nuestras politicas de privacidad</p>
            <h3>Ver Ahora!</h3>
            <a href="{{ asset('avisodeprivacidad.pdf') }}" target="_blank"><i class="fa fa-file"></i> Aviso de privacidad</a>
          </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                 <p class="text-muted small mb-0">&copy; HATZALAH - Todos los Derechos Reservados</p>
            </div>
            <div class="col-md-5">
                <ul class="nav small justify-content-start">
                    <li class="nav-item styling5"><a class="js-scroll-trigger" href="/#page-top">Inicio</a></li>
                    <li class="nav-item styling5"><a class="js-scroll-trigger" href="/#lior-suchard">Lior Suchard
</a></li>
                    <li class="nav-item styling5"><a class="js-scroll-trigger" href="/#location">Ubicación</a></li>
                    <li class="nav-item styling5"><a class="js-scroll-trigger" href="/#about">Acerca De</a></li>
                    <li class="nav-item styling5"><a class="js-scroll-trigger" href="/#tickets">Boletos</a></li>
                </ul>  
            </div>
            <div class="col-md-2">
              <a href="http://www.789.mx" target="_blank">
                <img src="{{ asset('img/789logo.png') }}" alt="www.789.mx" width="50px">
              </a>
            </div>
        </div>
    </div>
  </footer>
   <!-- Scroll to Top Button-->
  <a class="scroll-to-top js-scroll-trigger" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('page/js/hatzalah.min.js') }}"></script>
    @yield('scripts')
</body>
</html>
