<!DOCTYPE html>
<html>
<head>
<title>Boletos</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<meta charset="iso-8559-1">
<style>
	.bp-container{position:relative;background-color:#fff;width:98%;padding:1% 1%;font-family:Arial,Helvetica,sans-serif}.bp-box-left{position:absolute;top:0;left:0;width:400px;border:1px solid #d9d9d9;margin-top:1%}.bp-box-right{position:absolute;top:-5;left:310;width:47%}.clear{clear:both}.rectangulo-atencion{position:relative;margin:0;width:100%;height:150px;background:#b40500}.rectangulo-banquete{position:relative;margin:0;width:100%;height:150px;background:#FA8905}.rectangulo{position:relative;margin:0;width:100%;height:150px;background:#1087b2}.rectangulo2{position:relative;margin:0;width:100%;height:150px;background:#614474}.rectangulo3{position:relative;margin:0;width:100%;height:150px;background:#1dc6cd}.rectangulo4{position:relative;margin:0;width:100%;height:150px;background:#b40500}.rectangulo5{position:absolute;top:267;left:0;width:100%;height:180px;border:1px solid #d9d9d9;padding-bottom:8%}.rectangulo6{width:100%;height:auto;background:#175d99;position:absolute;top:-45;left:0}.for-title{background-color:#175d99;text-align:center;color:#fff;padding-bottom:5px;padding-top:5px}.title{color:#fff;padding-top:5px;padding-left:8px}.content1{display:inline-block;vertical-align:middle;text-align:center;width:45%}.content1 img{width:70px;height:auto}.content2{display:inline-block;vertical-align:middle;text-align:center;width:45%}.content2 p{color:#fff;font-size:14px}.content2 small{color:#fff}.content3{display:inline-block;vertical-align:middle;text-align:center;width:45%}.content3 p{color:#175d99;font-size:14px}.content3 small{color:#175d99}.content4{display:inline-block;vertical-align:middle;text-align:center;width:45%}.content4 img{width:120px;height:auto}.triangulo-equilatero-bottom-left{width:100%;height:0;padding-top:0%;padding-bottom:62%}.triangulo-equilatero-bottom-left div{width:0;height:0;border-right:180px solid transparent;border-top:300px solid transparent;border-left:200px solid #175d99;border-bottom:240px solid #175d99}.logo{position:absolute;top:160;right:5;padding-right:2%;margin-top:-30%}.title2{position:absolute;top:30;left:80;font-size:28px;font-weight:700;color:#777;padding-top:12%;padding-right:5%}.forma-margen{padding:5px}.forma{border:1px solid #fff;-webkit-border-radius:0 80px 0 80px;-moz-border-radius:0 80px 0 80px;border-radius:0 80px 0 80px;height:auto;background-color:#fff}.forma2{border:1px solid #b40500;-webkit-border-radius:0 80px 0 80px;-moz-border-radius:0 80px 0 80px;border-radius:0 80px 0 80px;height:auto;background-color:transparent}.responsive{width:100%;height:auto}h1{font-size:40px;letter-spacing:1px}.triangulo-sup2{width:100%;height:0;padding-top:0%;padding-bottom:10%;overflow:hidden}.triangulo-sup2 div{width:0;height:0;border-top:90px solid #175d99;border-left:677px solid transparent;-webkit-transform:rotate(360deg)}.tabla-fort{display:inline-block;padding-top:1%;padding-left:1%}.tabla-fort img{position:absolute;top:0;left:10}.numero{float:right;padding-right:10%}th,td{padding-top:15px;padding-left:5%;padding-right:20%}.color-table{color:#175d99}.letras th{font-weight:lighter}.letras2{font-size:14px;padding-left:3%}.letras2 span{color:#175d99}.letras3{padding-left:2%;display:inline-block;text-align:center}.letrasHatPresenta{margin-top:20px;padding-left:0;font-size:10px}.acceso{font-size:14px;margin-left:20px}.align-center{text-align:center}.margen{padding-right:10%;margin-left:-30%}.margen2{margin-top:0;margin-bottom:0}.margen-top{padding-top:5%}.margen3{vertical-align:middle}.table-rect{margin-top:45px;margin-left:10px}.table-asientos th{padding:10px;padding-top:0}.table-asientos td{padding:10px;padding-top:0}.fecha-letras{font-size:12px;margin-left:-35%}.codigoimg{margin-left:50%}.logo-plus img{position:absolute;top:3;left:5;width:270px}.logo-odn img{position:absolute;top:5;left:23;width:270px}.logo-mansur img{position:absolute;top:5;left:15;width:300px}.logo-atencion img{position:absolute;top:0;left:15;width:290px}.logo-banquetes img{position:absolute;top:0;left:15;width:300px}.logo-789 img{position:absolute;top:20;left:75;width:130px}
</style>
</head>
<body>
	<?php $cant = count($orden->boletos); $i = 0;?>
	@foreach($orden->boletos as $boleto)
	<?php $i++; ?>
	@if($i == $cant)
    <div class="bp-container">
	@else
	<div class="bp-container" style="page-break-after: always">
	@endif
		<div class="bp-box-left">
			<div class="triangulo-sup2"><div></div></div>
			<div class="title2">
				boletos.hatzalah.mx
			</div>
			<div class="triangulo-equilatero-bottom-left">
				<div></div>
			</div>
			<div class="logo"><img src="{{ public_path('img/boletos/logoazul.png') }}" height="120px"></div>
			<div style="position:relative">
				<div class="rectangulo6">
					<div class="forma-margen">
						<div class="forma">
							<div class="tabla-fort"><img src="{{ public_path('img/boletos/logoAzulChico.png') }}" height="50px"></div>
							<div class="tabla-fort numero"><p>{{ $orden->id }}</p></div>
							<div class="table-rect">
								<table class="table-asientos" style="white-space: nowrap;">
									<tr class="color-table">
										<th>Seccion</th>
										<th>Fila</th>
										<th>Asiento</th>
										<th>Precio</th>
									</tr>
									<?php 
										$p = 1;
										if ($boleto->id >= 342 && $boleto->id <= 785) {
											$p = 2;
										}
									?>
									<tr class="letras">
										<th>ZONA @if($orden->planta == 0) 1 @else {{ $orden->planta }} @endif - P{{ $p }}</th>
										<th>{{ $boleto->fila }}</th>
										<th>{{ $boleto->numero }}</th>
										@if ($orden->planta === '2')
										<th>$1,200.00</th>
										@else
										<th>$1,600.00</th>
										@endif
									</tr>
								</table>
							</div>
							<div class="forma-margen">
								<div class="forma2">
									<p class="letras2"><span><b>Nombre: </b></span>{{ $orden->name }}</p>
									<p class="letras2"><span><b>Numero de referencia: </b></span>{{ $boleto->id }}-{{ $orden->id }}</p>
									<div class="align-center">
										<div class="letras3 margen letrasHatPresenta">
											<p class="margen2">HATZALAH PRESENTA</p>
											<p class="margen2">LIOR SUCHARD</p>
											<p class="margen2">AUDITORIO RAFAEL Y REGINA KALACH</p>
											<p class="margen2">COLEGIO HEBREO MONTE SINAÍ</p>
										</div>
										<div class="letras3">
											<p class="letras2 margen2"><span><b>Acceso</b></span></p>
											<p class="margen2 acceso">Acceso 3</p>
										</div>
									</div>
									<div class="align-center margen-top">
										<div class="letras3 margen3 fecha-letras">
											<p>DOMINGO 22-09-19 19:00H</p>
										</div>
										<div class="letras3 margen3 codigoimg">
											{!! $barra::getBarcodeHTML($boleto->id . "789" . $orden->id, "EAN13", 1.3, 25) !!}
											{!! $barra::getBarcodeSVG($boleto->id . "789" . $orden->id, "EAN13", 1.3, 25) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="rectangulo5">
					<div class="for-title">
						<h4>Suscribete  a  nuestra  recaudacion  mensual</h4>
					</div>
					<div style="margin-top:35px;">
						<div class="content4 margen-top">
							<img src="{{ public_path('img/boletos/logoazul.png') }}">
						</div>
						<div class="content3 margen-top">
							<p>Ahora es mas facil con<br>tarjetas de credito y<br>facturacion automatica</p><br>
							<small><b>www.donativos.hatzalah.mx</b></small>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="bp-box-right">
			<div class="rectangulo">
				<p class="title">Este sistema fue creado con amor por:</p>
				<div class="logo-789">
					<img src="{{ public_path('img/boletos/logo789MX.png') }}">
				</div>
			</div>
			<div class="rectangulo2">
				<p class="title">Patrocinado por:</p>
				<div class="logo-plus">
					<img src="{{ public_path('img/boletos/plus_blanco.png') }}">
				</div>				
			</div>
			<div class="rectangulo3">
				<p class="title">Patrocinado por:</p>
				<div class="logo-odn">
					<img src="{{ public_path('img/boletos/odn_blanco.png') }}">
				</div>
			</div>
			<div class="rectangulo-atencion">
				<p class="title">Patrocinado por:</p>
				<div class="logo-atencion">
					<img src="{{ public_path('img/boletos/atencion_blanco.png') }}">
				</div>
			</div>
			<div class="rectangulo4">
				<p class="title">Patrocinado por:</p>
				<div class="logo-mansur">
					<img src="{{ public_path('img/boletos/mansur_blanco.png') }}">
				</div>
			</div>
			<div class="rectangulo-banquete">
				<p class="title">Patrocinado por:</p>
				<div class="logo-banquetes">
					<img src="{{ public_path('img/boletos/banquetes_atrib_blanco.png') }}">
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
    @endforeach
</body>
</html>
