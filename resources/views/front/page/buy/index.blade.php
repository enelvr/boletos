@extends('front.layouts.app')
@section('content')
<div class="bg-buy">
  <header>
   <div class="container">
     <div class="row pt-4">
         <div class="col-md-6">
           <div>
             <a href=""><img src="img/logo.png" class=" logo img-fluid"></a>
           </div>
         </div>
         <div class="col-md-6 boton-header">
             <a class="btn boton boton4" href="">Compra tus Boletos</a>
         </div>
     </div>
   </div>
   <div class="overlay"></div>
  </header>

	<section class="content-section text-center text-white">
    <div class="container">
      <div class="content-section-heading pb-5">
        <div class="row">
          <div class="col-sm-12">
            <h4 class="patrocinado-boletos">Patrocinado por:</h4>
          </div>
          <div class="col-md-1 col-sm-12"></div>
          <div class="col-md-2 col-sm-12">
              <img class="logo-patrocinante-boletos" src="{{ asset('img/boletos/plus_blanco.png') }}" alt="">
          </div>
          <div class="col-md-2 col-sm-12">
              <img class="logo-patrocinante-boletos" src="{{ asset('img/boletos/odn_blanco.png') }}" alt="">
          </div>
          <div class="col-md-2 col-sm-12">
              <img class="logo-patrocinante-boletos" src="{{ asset('img/boletos/mansur_blanco.png') }}" alt="">
          </div>
          <div class="col-md-2 col-sm-12">
              <img class="logo-patrocinante-boletos" src="{{ asset('img/boletos/atencion_blanco.png') }}" alt="">
          </div>
          <div class="col-md-2 col-sm-12">
              <img class="logo-patrocinante-boletos" src="{{ asset('img/boletos/banquetes_atrib_blanco.png') }}" alt="">
          </div>
          <div class="col-md-1 col-sm-12"></div>
        </div>
        <a class="btn c-boletos2">Compra tus Boletos</a>
      </div>

	  <form action="{{ route('compraBoletos') }}" method="POST" id="compra-boletos">
	  @csrf
      <!-- Seleccion de zona --> 
      <div class="row">
        <div class="col-md-4 offset-md-4">
            <div class="card bg-dark">
            	<div class="card-body">
                <h5 class="card-title text-center">Selecciona tu zona</h5>
                @if($disponibles2[0] != 0)
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="planta" value="1" id="platabajaCheck" checked>
                  <label class="form-check-label" for="platabajaCheck"> Zona 1 - $1,600 MXN</label>
                </div>
                @else
                <span>Zona 1 - SOLD OUT</span>
                @endif
                @if($disponibles2[1] != 0)
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="planta" value="2" id="plantaaltaCheck" @if($disponibles2[0] == 0) checked @endif>
                  <label class="form-check-label" for="plantaaltaCheck"> Zona 2 - $1,200 MXN</label>
                </div>
                @else
                <span>Zona 2 - SOLD OUT</span>
                @endif
            	</div>
            </div>
        </div>
      </div>

      <!-- fin Seleccion de zona --> 
      <br><br>

      <!-- Texto asignación--> 
      <div class="row">
        <div class="col-md-8 offset-md-2 text-dark">
            <div class="card bg-light">
            	<div class="card-body">
            		<h5 class="card-title text-center">Los asientos serán asignados<br>de abajo hacia arriba</h5>
                <h5 class="card-title text-center"><strong>Más rápida tu compra, mejor asiento te toca</strong></h5>
                <small>Evento recomendado para mayores de 12 años</small><br>
                <small>N.R.D.A</small>
                <hr>
                <small class="text-danger">No hay devoluciones, cambios ni reembolsos. en caso de cancelación del evento, <b>HATZALAH</b> se pondrá en contacto con cada comprador para llevar a cabo dicha devolución o cambios pertinentes.</small>
            	</div>
            </div>
        </div>
      </div>
       <!-- fin Texto asignación--> 
      <br><br>

      {{-- <!-- Metodo de pago--> 
      <div class="row">
        <div class="col-md-4 offset-md-4">
            <div class="card bg-dark">
            	<div class="card-body">
            		<h5 class="card-title text-center">Selecciona tu Metodo de pago</h5>
            		
            		<div class="text-center">
                  <label>
                    <input type="radio" name="metodoPago" value="paypal" checked>
                    <img src="{{ asset('page/img/paypal.jpg') }}" class="img-fluid" width="100px" alt="pagar con paypal">
                  </label>
                  <label>
                    <input type="radio" name="metodoPago" value="mercadoPago">
                    <img src="{{ asset('page/img/mercadopago.png') }}" class="img-fluid"width="100px" alt="pagar con mercado de pago">
                  </label>
                </div>
            	</div>
            </div>
        </div>
      </div>
      <!-- finMetodo de pago-->  --}}

      <br><br>

      <!-- Datos para pago--> 
      <div class="row">
        <div class="col-md-8 offset-md-2 text-dark">
            <div class="card bg-light" style="background-color: #ffffffe0 !important">
              @if (session('errors'))
              <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ session('errors') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
            	<div class="card-body">
            		<h5 class="card-title">
                  <b>Ingresa tus datos</b>
                </h5>
                <div>
                  Total a pagar: <span id="totalPagar">$0</span> MXN
                </div>
                <br>
                <div class="form-group">
                  <label for="nombre" class="form-label">NOMBRE</label>
                  <input type="text" class="form-control form-input" id="nombre" name="nombre" placeholder="Nombre Completo">
                  <small id="nombreValid" class="form-text" style="color:red; display: none; text-align: left">Por favor ingrese su nombre</small>
                </div>
                <div class="form-group">
                  <label for="email" class="form-label">CORREO ELECTRÓNICO</label>
                  <input type="email" class="form-control form-input" id="email" name="email" placeholder="ejemplo@gmail.com">
                  <small id="emailValid" class="form-text" style="color:red; display: none; text-align: left">Por favor ingrese su correo electrónico</small>
                </div>
                <div class="form-group">
                  <label for="telefono" class="form-label">TELÉfONO</label>
                  <input type="text" class="form-control form-input" id="telefono" name="telefono" placeholder="55 555 555" onkeypress="javascript:return isNumber(event)">
                  <small id="tlfValid" class="form-text" style="color:red; display: none; text-align: left">Por favor ingrese su número de telefono</small>
                </div>
                <div class="form-group">
                  <label for="cantidad" class="form-label">CANTIDAD</label>
                  <input type="number" class="form-control form-input" id="cantidad" name="cantidad" placeholder="0" value="1" min="1" max="8" step="1">
                  <small id="cantValid" class="form-text" style="color:red; display: none; text-align: left">Por favor ingrese una cantidad superior a 1</small>
                  <small id="cantValid2" class="form-text" style="color:red; display: none; text-align: left">El maximo de boletos superado. El maximo es <span id="maximovalid"></span></small>
                </div>
                <div class="text-center">
                  <button id="comprar" type="submit" class="btn boton6" style="display: none; background: rgb(29, 198, 205); color: white;">COMPRAR BOLETO</button>
                  <div id="paypal-button"></div>
                </div>
            	</div>
            </div>
        </div>
      </div>
      <!-- fin Datos para pago--> 
	  </form>
    </div>
  </section>
</div>
@endsection
@section('scripts')
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  $('input[name="metodoPago"]').change(function() {
    var metodopago = $('input[name="metodoPago"]:checked').val();
    if(metodopago === 'paypal') {
      console.log('paypal')
      $('#paypal-button').css('display', 'inline-block');
      $('#comprar').css('display', 'none');
    } else {
      console.log('mercado pago')
      $('#comprar').css('display', 'inline-block');
      $('#paypal-button').css('display', 'none');
    }
  });

  function setTotal () {
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2
    })

    var monto = $('input[name="planta"]:checked').val() === '1' ? 1600 : 1200;
    var cantidad = $('input[name="cantidad"]').val();
    var total = monto * cantidad;
    $('#totalPagar').html(formatter.format(total));
  }

  function isNumber(evt) {
      var iKeyCode = (evt.which) ? evt.which : evt.keyCode
      if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
          return false;
      return true;
  }

  function validarForm () {
    var nombre = $('input[name="nombre"]').val();
    var email = $('input[name="email"]').val();
    var telefono = $('input[name="telefono"]').val();
    var cantidad = $('input[name="cantidad"]').val();
    var msj = 'Ingrese todos los datos requeridos';
    var validar = true;
    if (nombre === '') {
      $('#nombreValid').css('display', 'block');
      validar = false;
    } else {
      $('#nombreValid').css('display', 'none');
    }
    if (email === '') {
      $('#emailValid').css('display', 'block');
      validar = false;
    } else {
      $('#emailValid').css('display', 'none');
    }
    if (telefono === '') {
      $('#tlfValid').css('display', 'block');
      validar = false;
    } else {
      $('#tlfValid').css('display', 'none');
    }
    if (cantidad < 1) {
      $('#cantValid').css('display', 'block');
      validar = false;
    } else {
      $('#cantValid').css('display', 'none');
    }
    const selected = $('input[name="planta"]:checked').val();
    const disponible = disponibles[selected - 1];
    if (cantidad > disponible) {
      $('#cantValid2').css('display', 'block');
      $('#maximovalid').html(disponible);
      validar = false;
    } else {
      $('#cantValid2').css('display', 'none');
    }
    return validar;
  }

  function updateMaxCant() {
      const selected = $('input[name="planta"]:checked').val();
      const disponible = disponibles[selected - 1];
      if (disponible < 8) {
        $('input[name="cantidad"]').attr('max', disponible);
      } else {
        $('input[name="cantidad"]').attr('max', 8);
      }
  }
  const disponibles = JSON.parse('{!! $disponibles !!}');

  setTotal();
  updateMaxCant();

  $('#compra-boletos').submit(function(e){
    if (!validarForm()) {
      e.preventDefault();
    }
  }); 

  $('input[name="planta"]').change(function() {
    setTotal();
    updateMaxCant();
  });
  
  $('input[name="cantidad"]').change(function() {
    setTotal();
  });

  $('input[name="cantidad"]').keyup(function() {
    setTotal();
  });

  paypal.Button.render({
    style: {
      size: 'medium',
      color: 'gold',
      shape: 'rect',
      label: 'checkout',
      tagline: 'true'
    },

    env: 'production', // Or 'production'
    // Set up the payment:
    // 1. Add a payment callback
    payment: function(data, actions) {
      validarForm()
      var nombre = $('input[name="nombre"]').val();
      var email = $('input[name="email"]').val();
      var telefono = $('input[name="telefono"]').val();
      var cantidad = $('input[name="cantidad"]').val();
      var planta = $('input[name="planta"]:checked').val();
      // 2. Make a request to your server
      return actions.request.post('/api/create-payment', {
        nombre: nombre,
        email: email,
        telefono: telefono,
        cantidad: cantidad,
        planta: planta,
      })
        .then(function(res) { 
          // 3. Return res.id from the response
          return res.id;
        });
    },
    // Execute the payment:
    // 1. Add an onAuthorize callback
    onAuthorize: function(data, actions) {
      var returnurl = data.returnUrl.split('?')[0];
      var ordenid = returnurl.split('/')[4];
      // 2. Make a request to your server
      return actions.request.post('/api/execute-payment', {
        paymentID: data.paymentID,
        payerID:   data.payerID,
      })
        .then(function(res) {
          // 3. Show the buyer a confirmation message.
          console.log(res);
          if (res.state == 'approved') {
            const url = res.redirect_urls.return_url + '&invoice=' + res.transactions[0].invoice_number + '&transaction=' +  res.transactions[0].related_resources[0].sale.id;
            window.location.href = url;
          }
        });
    }
  }, '#paypal-button');
</script>
@endsection