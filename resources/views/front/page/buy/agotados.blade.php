@extends('front.layouts.app')
@section('content')
<div class="bg-buy">
  <header>
   <div class="container">
     <div class="row pt-4">
         <div class="col-md-6">
           <div>
             <a href=""><img src="img/logo.png" class=" logo img-fluid"></a>
           </div>
         </div>
         <div class="col-md-6 boton-header">
         </div>
     </div>
   </div>
   <div class="overlay"></div>
  </header>

	<section class="content-section text-center text-white">
    <div class="container">
      <!-- Texto asignación--> 
      <div class="row">
        <div class="col-md-8 offset-md-2 text-dark">
            <div class="card bg-light">
            	<div class="card-body">
            		<h5 class="card-title text-center">Boletos Agotados</h5>
            	</div>
            </div>
        </div>
      </div>
       <!-- fin Texto asignación--> 
    </div>
  </section>
</div>
@endsection