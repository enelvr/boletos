@extends('front.layouts.app')
@section('content')
<div class="bg-buy">
  <header>
   <div class="container">
     <div class="row pt-4">
         <div class="col-md-6">
           <div>
             <a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" class=" logo img-fluid"></a>
           </div>
         </div>
         <div class="col-md-6 boton-header">
             <a class="btn boton boton4" href="{{ route('boletos') }}">Compra tus Boletos</a>
         </div>
     </div>
   </div>
   <div class="overlay"></div>
 </header>
	<section class="content-section text-center text-white">
    <div class="container">
      <!-- Texto asignación--> 
      <div class="row">
        <div class="col-md-8 offset-md-2 text-dark">
            <div class="card bg-light">
            	<div class="card-body">
            		<h5 class="card-title text-center">{{ $comprador }}, su compra ha finalizado con exito</h5>
                    <h5 class="card-title text-center"><strong>Zona {{ $planta }}</strong></h5>
                    <h5 class="card-title text-center"><strong>Asientos asignados: {{ $boletosAsignados }}</strong></h5>
                    <div style="color: #2aa0b3">
                      <small class="card-title text-center">Una copia de los boletos será enviada vía correo electrónico</small><br>
                      {{-- <small class="card-title text-center">Para descargar tus boletos da <a href="{{ route('downloadBoletos', $ordenid) }}" style="color: #2aa0b3" >click aquí</a></small> --}}
                      <form action="{{ route('downloadBoletos') }}" method="POST" id="compra-boletos">
                        @csrf
                      <input type="hidden" name="ordenid" value="{{ $ordenid }}">
                        <small class="card-title text-center">Para descargar tus boletos da <button type="submit" class="btnLink">click aquí</button></small>
                      </form>
                    </div>
            	</div>
            </div>
        </div>
      </div>
       <!-- fin Texto asignación-->
    </div>
  </section>
</div>
@endsection
