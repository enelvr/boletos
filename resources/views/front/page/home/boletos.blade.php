@extends('front.layouts.app')
@section('content')
<div id="colorlib-page">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" colorlib-navbar-brand">
                        <a class="colorlib-logo" href="index.html"><img src="img/logo.png" class=" logo img-fluid"></a>
                    </div>
                    <div class="boton-header">
                        <button class="boton">Compra tus Boletos</button>
                    </div>
                    <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
                </div>
            </div>
        </div>
    </header>
    <div class="js-fullheight fondo">
    </div>
    <div style="width:50%; margin: auto">
        <form action="{{ route('compraBoletos') }}" method="POST">
            @csrf

            Selecciona tu zona
            <div class="form-check">
                <input class="form-check-input" type="radio" name="planta" id="plantaBaja" value="baja" checked>
                <label class="form-check-label" for="plantaBaja">
                    Planta Baja - $ 1,600 mxn
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="planta" id="plantaAlta" value="alta">
                <label class="form-check-label" for="plantaAlta">
                    Planta Alta - $ 1,200 mxn
                </label>
            </div>
            <br>
            Metodo de Pago
            <div class="form-check">
                <input class="form-check-input" type="radio" name="pago" id="Paypal" value="paypal" checked>
                <label class="form-check-label" for="paypal">
                    Paypal
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="pago" id="mercadoPago" value="mercadoPago">
                <label class="form-check-label" for="mercadoPago">
                    MercadoPago
                </label>
            </div>
            <br>
            Ingresa tus datos
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese su nombre">
            </div>
            <div class="form-group">
                <label for="email">Correo Electronico</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Ingrese su correo electronico">
            </div>
            <div class="form-group">
                <label for="telefono">Telefono</label>
                <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese su numero de telefono">
            </div>
            <div class="form-group">
                <label for="cantidad">Cantidad de Boletos</label>
                <input type="number" min="0" step="1" class="form-control" id="cantidad" name="cantidad" placeholder="Ingrese cantidad de boletos">
            </div>
            <button type="submit" class="btn btn-primary">Comprar Boletos</button>
        </form>
    </div>
</div>
@endsection