@extends('front.layouts.app')
@section('content')
<!-- Header -->
  <header class="masthead d-flex">
    <div class="container">
      <div class="row">
          <div class="col-md-6">
            <div>
              <a href=""><img src="img/logo.png" class=" logo img-fluid"></a>
            </div>
          </div>
          <div class="col-md-6 boton-header">
              <a href="{{ route('boletos') }}" class="btn boton">Compra tus Boletos</a>
          </div>
      </div>
    <div class="text-center my-auto margen3">
      <div class="row">
        <div class="col-md-10 col-sm-12">
          <h3 class="styling">
            <em>SUPERNATURAL ENTERTAINMENT</em>
          </h3>
          <h1 class="mb-2 styling2 sombra">LIOR SUCHARD</h1>
          <h3 class="mb-1 styling3">
            <p>AUDITORIO RAFAEL Y REGINA KALACH <br> Colegio Hebreo Monte Sinaí</p>
            <p>22 de Septiembre 2019</p>
          </h3>
          <a class="btn c-boletos" href="{{ route('boletos') }}">Compra tus Boletos</a>
        </div>
        <div class="col-md-2 col-sm-12 patrocinantes">
          <h5 style="white-space: nowrap;">Patrocinado por:</h5>
          <img class="logo-patrocinante" src="{{ asset('img/boletos/plus_blanco.png') }}" alt="">
          <img class="logo-patrocinante" src="{{ asset('img/boletos/odn_blanco.png') }}" alt="">
          <img class="logo-patrocinante" src="{{ asset('img/boletos/mansur_blanco.png') }}" alt="">
          <img class="logo-patrocinante" src="{{ asset('img/boletos/atencion_blanco.png') }}" alt="">
          <img class="logo-patrocinante" src="{{ asset('img/boletos/banquetes_atrib_blanco.png') }}" alt="">
        </div>
      </div>
    </div>
    </div>
    <div class="overlay"></div>
  </header>

    <!-- LIOR SUCHAR -->
  <section class="content-section bg-primary text-white" id="lior-suchard">
    <div class="container">
      <div class="">
        <h2 class="mb-3 styling4">LIOR SUCHARD</h2>
      </div>
      <div class="row">
        <div class="col-md-12">
            <p class="letras">(6 de Diciembre de de 1981 - Haifa, Israel)</p>
            <p class="letras">Lior Suchard es un mentalista israeli que interpreta entretenimiento sobrenatural</p><br>
            <p class="letras">¿Crees en la lectura de mentes? Después de ver a Lior Suchard en acción es muy dificil no creer El puede indagar hasta tus pensamientos mas profundos y saber lo que voy a decir antes de que abras la boca. Su impresionante actuación de lectura de mentes, influencia de pensamientos predicción y telequinesis le ha otorgado reconocimiento Internacional como un artista supernatural y mentalista de clase superior.</p>
        </div>
      </div>
    </div>
  </section>
   
  <!-- Map location-->
  <section id="location" class="map">
    <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=COLEGIO+HEBREO+MONTE+SINAI+A.C&amp;aq=0&amp;sll=19.3843269,-99.2761225&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=COLEGIO+HEBREO+MONTE+SINAI+A.C&amp;t=m&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe>
    <br />
    <small>
      <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=COLEGIO+HEBREO+MONTE+SINAI+A.C.A&amp;aq=0&amp;sll=19.3843269,-99.2761225&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=OLEGIO+HEBREO+MONTE+SINAI+A.C&amp;t=m&amp;z=16&amp;iwloc=A"></a>
    </small>
  </section>

  <!-- acerca de -->
  <section id="about" class="content-section bg-primary text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="styling4">DÓNDE</h2>
                <p class="styling3">Av. Loma de la Palma 133</p>
                <p class="styling3">Lomas de Vista Hermosa</p>
                <p class="styling3">05109</p>
                <p class="styling3">Ciudad de México, CDMX</p>
            </div>
            <div class="col-md-6">
                <h2 class="styling4">CUÁNDO</h2>
                <p class="styling3">Domingo 22 de Septiembre</p>
                <p class="styling3">2019</p>
                <p class="styling3">19:00 hrs</p>    
            </div>
        </div>
    </div>
  </section>

  <!-- boletos -->
 <section id="tickets" class="callout">
    <div class="container text-center">
      <a class="btn tickets" href="{{ route('boletos') }}">COMPRA TUS BOLETOS</a>
        <img class="img-fluid" src="{{ asset('page/img/tickets.png') }}">
      <h3>El dinero recaudado de la venta de boletos
será donado a Hatzalah</h3>
    </div>
  </section>
@endsection